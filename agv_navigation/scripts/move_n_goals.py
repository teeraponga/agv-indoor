#!/usr/bin/env python3

from actionlib_msgs.msg import GoalStatusArray
from geometry_msgs.msg import PoseStamped

import rospy
import time

"""
have a list of n goals and home position at index 0
set goal_loop boolean to
 - True : loop between all goals except home position
 - False : return to home position after finish all goals
"""

goal_0 = PoseStamped()
goal_0.header.frame_id = "map"
goal_0.pose.orientation.z = 0.0
goal_0.pose.orientation.w = 1.0

goal_1 = PoseStamped()
goal_1.header.frame_id = "map"
goal_1.pose.position.x = 7.5
goal_1.pose.position.y = -3.2
goal_1.pose.orientation.z = -0.66
goal_1.pose.orientation.w = 0.75

goal_2 = PoseStamped()
goal_2.header.frame_id = "map"
goal_2.pose.position.x = 8.5
goal_2.pose.position.y = -12.5
goal_2.pose.orientation.z = 0.73
goal_2.pose.orientation.w = 0.67

goal_3 = PoseStamped()
goal_3.header.frame_id = "map"
goal_3.pose.position.x = 19.0
goal_3.pose.position.y = -11.5
goal_3.pose.orientation.z = 0.77
goal_3.pose.orientation.w = 0.64

goal_4 = PoseStamped()
goal_4.header.frame_id = "map"
goal_4.pose.position.x = 15.5
goal_4.pose.position.y = -0.5
goal_4.pose.orientation.z = -1.0
goal_4.pose.orientation.w = 0.0

goal_list =[
    goal_0,
    goal_1,
    goal_2,
    goal_3,
    goal_4
]

current_goal = 1
goal_reached = False
goal_sent = False
goal_loop = True
return_home = False
goal_pub = rospy.Publisher("/move_base_simple/goal", PoseStamped, queue_size=10)

def goal_status_callback(data):
    global current_goal, goal_reached, goal_sent
    if(len(data.status_list)==0):
        return
    elif(data.status_list[-1].status==1):
        goal_sent = True
    elif(data.status_list[-1].status==3 and goal_sent):
        goal_reached = True
        current_goal+=1
        print("goal reached")
        time.sleep(3)
        send_goal()
    elif(data.status_list[-1].status==4 and goal_sent):
        print("goal stuck, re-send goal")
        time.sleep(3)
        send_goal()
    

def send_goal():
    global current_goal, goal_pub, goal_sent, goal_loop, return_home
    global goal_list
    if(return_home):
        exit()
    if(current_goal>len(goal_list)-1):
        if(not goal_loop):
            goal = goal_list[0]
            goal.header.stamp = rospy.Time.now()
            goal_pub.publish(goal)
            goal_sent = False
            return_home = True
            print("return to home")
        else:
            current_goal = 1
            send_goal()
    else:
        goal = goal_list[current_goal]
        goal.header.stamp = rospy.Time.now()
        goal_pub.publish(goal)
        goal_sent = False
        print("send goal {}".format(current_goal))


if(__name__=="__main__"):
    rospy.init_node("go_2_goals")
    goal_status_sub = rospy.Subscriber("move_base/status", GoalStatusArray, goal_status_callback)
    time.sleep(2)
    send_goal()
    time.sleep(4)
    # while(not rospy.is_shutdown()):
    rospy.spin()