#!/usr/bin/env python3

import rospy
import rospkg
import yaml
import tf
from os.path import expanduser

from geometry_msgs.msg import Pose

from agv_navigation.srv import save_position_srv

from agv_backend.srv import marker_array

import time

"""
Append robot's current position into yaml file
"""

home = expanduser("~")
class save_position:
    def __init__(self) -> None:
        pkg = rospkg.RosPack()

        self.sub = rospy.Subscriber("/robot_pose", Pose, self.pos_callback)
        self.current_pose = Pose()

        self.file_path = "{}/saved_positions/test.yaml".format(pkg.get_path('agv_navigation'))

        self.service = rospy.Service('save_position', save_position_srv, self.service_callback)

        self.after_service = rospy.ServiceProxy('get_marker', marker_array)

    def pos_callback(self, data):
        "update robot's current position "
        self.current_pose = data

    def service_callback(self, req):
        "when call, save current position into yaml file for later use"
        with open(self.file_path, 'r') as f:
            yaml_dict = yaml.safe_load(f) or {}
        new_pose = {
            "pos_{}".format(len(yaml_dict.keys())) : {
                "px" : self.current_pose.position.x,
                "py" : self.current_pose.position.y,
                "qz" : self.current_pose.orientation.z,
                "qw" : self.current_pose.orientation.w
            }
        }
        yaml_dict.update(new_pose)
        with open(self.file_path, 'w') as f:
            yaml.dump(yaml_dict, f)

        time.sleep(0.5)
        try:
            resp = self.after_service()
            return resp
        except rospy.ServiceException as e:
            print("Service call failed: %s"%e)
    

if(__name__=="__main__"):
    rospy.init_node("save_position")
    save_position()
    rospy.spin()
    pass