#!/usr/bin/env python3

import rospy

from actionlib_msgs.msg import GoalStatusArray
from std_srvs.srv import Empty

"""
call service clear costmap in a defined interval
"""

def status_callback(data):
    pass

if(__name__=="__main__"):
    rospy.init_node("costmap_clearer")

    rospy.wait_for_service('/move_base/clear_costmaps')
    status_sub = rospy.Subscriber("move_base/status",GoalStatusArray, status_callback)
    service_proxy = rospy.ServiceProxy('/move_base/clear_costmaps', Empty)

    rate = rospy.Rate(0.13)

    while(not  rospy.is_shutdown()):
        service_proxy.call()
        print('clear')
        rate.sleep()