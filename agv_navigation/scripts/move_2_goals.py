#!/usr/bin/env python3

from actionlib_msgs.msg import GoalStatusArray
from geometry_msgs.msg import PoseStamped

import rospy
import time

"""
Loop between 2 goal_1 and goal_2
"""

goal_1 = PoseStamped()
goal_1.header.frame_id = "map"
goal_1.pose.position.x = 3.0
goal_1.pose.orientation.w = 1.0

goal_2 = PoseStamped()
goal_2.header.frame_id = "map"
goal_2.pose.orientation.w = 1.0

current_goal = 1
goal_reached = False
goal_sent = False

goal_pub = rospy.Publisher("/move_base_simple/goal", PoseStamped, queue_size=10)

def goal_status_callback(data):
    global current_goal, goal_reached, goal_sent
    if(len(data.status_list)==0):
        return
    elif(data.status_list[-1].status==1):
        goal_sent = True
    elif(data.status_list[-1].status==3 and goal_sent):
        goal_reached = True
        current_goal = 3 - current_goal
        print("goal reached")
        time.sleep(3)
        send_goal()
    elif(data.status_list[-1].status==4 and goal_sent):
        # current_goal = 3 - current_goal
        print("goal stuck, re-send goal")
        time.sleep(3)
        send_goal()
    
    pass


def send_goal():
    global current_goal, goal_pub, goal_sent
    global goal_1, goal_2
    if(current_goal==1):
        goal_1.header.stamp = rospy.Time.now()
        goal_pub.publish(goal_1)
        goal_sent = False
        print("send goal 1")
    elif(current_goal==2):
        goal_2.header.stamp = rospy.Time.now()
        goal_pub.publish(goal_2)
        goal_sent = False
        print("send goal 2")

    pass


if(__name__=="__main__"):
    rospy.init_node("go_2_goals")
    goal_status_sub = rospy.Subscriber("move_base/status", GoalStatusArray, goal_status_callback)
    time.sleep(2)
    goal_1.header.stamp = rospy.Time.now()
    goal_pub.publish(goal_1)
    time.sleep(4)
    # while(not rospy.is_shutdown()):
    rospy.spin()