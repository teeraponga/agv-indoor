#!/usr/bin/env python3

"""
subscribe tf for {map --> base_link} then publish as marker message
"""

from yaml.error import Mark
from visualization_msgs.msg import Marker
import tf
import rospy

marker_msg = Marker()
marker_msg.header.frame_id = "map"
marker_msg.color.g = marker_msg.color.a = 1.0
marker_msg.lifetime.secs = 1
marker_msg.type = 2
# marker_msg.mesh_resource = "package://agv_description/meshes/dome.stl"
marker_msg.scale.x = 0.5
marker_msg.scale.y = 0.5
marker_msg.scale.z = 0.5

if(__name__=="__main__"):
    rospy.init_node("pose_to_marker")
    listener = tf.TransformListener()

    marker_pub = rospy.Publisher('visualization_marker', Marker ,queue_size=1)

    rate = rospy.Rate(10.0)
    while not rospy.is_shutdown():
        try:
            (trans,rot) = listener.lookupTransform('/map', '/base_link', rospy.Time(0))
        except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
            continue

        marker_msg.header.stamp = rospy.Time.now()
        marker_msg.pose.position.x = trans[0]
        marker_msg.pose.position.y = trans[1]
        marker_msg.pose.position.z = trans[2]
        marker_msg.pose.orientation.x = rot[0]
        marker_msg.pose.orientation.y = rot[1]
        marker_msg.pose.orientation.z = rot[2]
        marker_msg.pose.orientation.w = rot[3]

        marker_pub.publish(marker_msg)

        rate.sleep()