#!/usr/bin/env python3

import roslibpy
import rospy
import time

def callback(data):
    print(data)
    # print(type(data['data']))
    # print("w:{0}, h:{1}, data:{2}".format(  data['info']['width'] , data['info']['height'], data['info']['width'] * data['info']['height']))
    pass

client = roslibpy.Ros(host='10.42.0.1', port=9090)
# client = roslibpy.Ros(host='10.42.0.1', port=9090)
client.run()

print(client.is_connected)

# listener = roslibpy.Topic(client, '/map', 'nav_msgs/OccupancyGrid')

# listener = roslibpy.Topic(client, '/robot_pose', 'geometry_msgs/Pose')
listener = roslibpy.Topic(client, '/command', 'std_msgs/String')
listener.subscribe(callback)

msg = roslibpy.Message({
    'linear':{'x': 0.0, 'y': 0.0, 'z': 0.0},
    'angular': {'x': 0.0, 'y': 0.0, 'z': 0.0}
})
talker = roslibpy.Topic(client, '/joy_vel', 'geometry_msgs/Twist')

msg2 = roslibpy.Message({'data':'slam_start' })
# msg2 = roslibpy.Message({'data':'slam_stop' })
# msg2 = roslibpy.Message({'data':'save_map' })
msg2 = roslibpy.Message({'data':'navigation_start' })
# msg2 = roslibpy.Message({'data':'navigation_stop' })

talker2 = roslibpy.Topic(client, '/command', 'std_msgs/String')
talker.publish(msg)
try:
    while True:
        # talker.publish(msg)
        time.sleep(1)
        pass
except KeyboardInterrupt:
    client.terminate()