#!/usr/bin/env python3
#  

"""
Subscribe robot position from tf
then publish as Pose/PoseStamped/Marker message
"""

from yaml.error import Mark
import rospy
import math
import tf
from geometry_msgs.msg import Pose, PoseStamped
from visualization_msgs.msg import Marker
from std_msgs.msg import Bool
# from geometry_msgs.msg import PoseWithCovarianceStamped

if __name__ == '__main__':
    rospy.init_node('tf_to_pose')

    listener = tf.TransformListener()

    ### Pose message ###
    pub = rospy.Publisher("/robot_pose", Pose, queue_size=10)
    msg = Pose()
    ### ### ###
    
    ### PoseStamped message ###
    pub2 = rospy.Publisher("/robot_posestamped", PoseStamped, queue_size=10)
    msg2 = PoseStamped()
    msg2.header.frame_id = "base_footprint"
    ### ### ###

    ### Marker message ###
    pub3 = rospy.Publisher("/robot_marker", Marker, queue_size=10)
    msg3 = Marker()
    msg3.header.frame_id = "map"
    msg3.id = 1000
    msg3.color.g = msg3.color.a = 1.0
    msg3.lifetime.secs = 1
    msg3.type = 1
    msg3.scale.x = 0.6
    msg3.scale.y = 0.5
    msg3.scale.z = 0.5
    ### ### ###

    pub_goal_status = rospy.Publisher("/goal_reached", Bool, queue_size=10)
    msg_goal_status = Bool()
    msg_goal_status.data = True

    # pub = rospy.Publisher("/robot_pose", PoseWithCovarianceStamped, queue_size=10)
    # msg = PoseWithCovarianceStamped()

    # rospy.wait_for_service('spawn')
    # spawner = rospy.ServiceProxy('spawn', turtlesim.srv.Spawn)
    # spawner(4, 2, 0, 'turtle2')

    # turtle_vel = rospy.Publisher('turtle2/cmd_vel', geometry_msgs.msg.Twist,queue_size=1)

    # msg.header.frame_id = "base_footprint"

    rate = rospy.Rate(10.0)
    while not rospy.is_shutdown():
        try:
            (trans,rot) = listener.lookupTransform('/map', '/base_footprint', rospy.Time(0))
            # print(trans)
            # print(rot)
            # print("--------------------")
            # msg.header.stamp = rospy.Time.now()
            # msg.pose.pose.position.x = trans[0]
            # msg.pose.pose.position.y = trans[1]
            # msg.pose.pose.position.z = trans[2]
            # msg.pose.pose.orientation.x = rot[0]
            # msg.pose.pose.orientation.y = rot[1]
            # msg.pose.pose.orientation.z = rot[2]
            # msg.pose.pose.orientation.w = rot[3]
            msg.position.x = trans[0]
            msg.position.y = trans[1]
            msg.position.z = trans[2]
            msg.orientation.x = rot[0]
            msg.orientation.y = rot[1]
            msg.orientation.z = rot[2]
            msg.orientation.w = rot[3]
            
            msg2.header.stamp = msg3.header.stamp = rospy.Time.now()
            msg2.pose = msg3.pose = msg
            
            pub.publish(msg)

            pub2.publish(msg2)

            pub3.publish(msg3)
        except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
            continue

        # angular = 4 * math.atan2(trans[1], trans[0])
        # linear = 0.5 * math.sqrt(trans[0] ** 2 + trans[1] ** 2)
        # cmd = geometry_msgs.msg.Twist()
        # cmd.linear.x = linear
        # cmd.angular.z = angular
        # turtle_vel.publish(cmd)

        rate.sleep()