#!/usr/bin/env python3

"""
launch/terminate ros launch from command
listen to topic /command
"""

import rospy
import roslaunch
import rospkg
import time

import yaml

from os.path import expanduser
from std_msgs.msg import String, Bool

class process_watcher:
    def __init__(self) -> None:
        pkg = rospkg.RosPack()

        ### get lidar topic and angle to remove ###
        with open( "{0}/config/package_name.yaml".format(pkg.get_path('agv_backend')), "r") as stream:
            try:
                config = yaml.safe_load(stream)
                slam = config['slam']
                navigation = config['navigation']
                
            except yaml.YAMLError as exc:
                print(exc)
        ### ### ### ###
        
        self.agv_slam_path = pkg.get_path(slam)
        self.agv_slam_launch_file = "agv_slam_cartographer.launch"
        self.slam_started = False
        self.slam_running = False

        self.agv_navigation_path = pkg.get_path(navigation)
        self.agv_navigation_launch_file = "agv_navigation_cartographer.launch"
        self.navigation_started = False
        self.navigation_running = False

        self.agv_save_map_path = pkg.get_path(slam)
        self.agv_save_map_launch_file = "save_map.launch"

        self.map_name = ""

        ### TEST ###
        # self.agv_slam_path = pkg.get_path("agv_gazebo")
        # self.slam_started = False
        # self.slam_running = False

        # self.agv_navigation_path = pkg.get_path("agv_gazebo")
        # self.navigation_started = False
        # self.navigation_running = False

        # self.agv_save_map_path = pkg.get_path("agv_slam")
        
        ### ### ### ###

        self.command_sub = rospy.Subscriber('/command', String, self.command_callback)
        self.command = ""

    def slam_start(self):
        """launch slam given .launch file path"""
        print("Starting SLAM")
        
        uuid = roslaunch.rlutil.get_or_generate_uuid(None, False)
        print("{0}/launch/{1}".format(self.agv_slam_path, self.agv_slam_launch_file))
        self.agv_slam_launch = roslaunch.parent.ROSLaunchParent(uuid, ["{0}/launch/{1}".format(self.agv_slam_path, self.agv_slam_launch_file)])
        
        self.agv_slam_launch.start()
        self.slam_started = True
        pass

    def slam_shutdown(self):
        """terminate slam node"""
        print("Shutting down SLAM")
        self.agv_slam_launch.shutdown()
        self.slam_started = False
        pass

    def save_map(self ,map_name='map'):
        """save map, default name is map"""
        print("Saving map")

        uuid = roslaunch.rlutil.get_or_generate_uuid(None, False)
        print("{0}/launch/{1}".format(self.agv_save_map_path, self.agv_save_map_launch_file))
        self.map_name = rospy.get_param("map", default="map")
        self.agv_save_map_launch = roslaunch.parent.ROSLaunchParent(uuid, 
        [(
            "{0}/launch/{1}".format(self.agv_save_map_path, self.agv_save_map_launch_file),
            [
                "filename:={0}".format(self.map_name)
            ]
        )])

        self.agv_save_map_launch .start()
        pass

    def navigation_start(self):
        """start navigation given .launch file"""
        print("Starting Navigation")

        uuid = roslaunch.rlutil.get_or_generate_uuid(None, False)
        print("{0}/launch/{1}".format(self.agv_navigation_path, self.agv_navigation_launch_file))
        self.map_name = rospy.get_param("map", default="map")
        self.agv_navigation_launch = roslaunch.parent.ROSLaunchParent(uuid, 
        [(
            "{0}/launch/{1}".format(self.agv_navigation_path, self.agv_navigation_launch_file),
            [
                "map_name:={0}".format(self.map_name)
            ]
        )])
        
        self.agv_navigation_launch.start()
        self.navigation_started = True
        pass

    def navigation_shutdown(self):
        """terminate navigation node"""
        print("Shutting down Navigation")
        self.agv_navigation_launch.shutdown()
        self.navigation_started = False
        pass

    def check_slam_node(self):
        pass

    def check_navigation_node(self):
        pass

    def command_callback(self, data):
        """subscribe to command topic"""
        print("received command: {0}".format(data.data))
        self.command = data.data
        


if(__name__=="__main__"):
    rospy.init_node('process_manager')
    rate = rospy.Rate(10)
    p = process_watcher()
    print("initialized..")
    while(not rospy.is_shutdown()):

        if(p.command==""):
            continue
        elif(p.command=="slam_start" and not p.slam_started):
            p.slam_start()
        elif(p.command=="save_map" and p.slam_started):
            p.save_map('map')
        elif(p.command=="slam_stop" and p.slam_started):
            p.slam_shutdown()
        elif(p.command=="navigation_start" and not p.navigation_started):
            p.navigation_start()
        elif(p.command=="navigation_stop" and p.navigation_started):
            p.navigation_shutdown()
        
        p.command = ""
        rate.sleep()