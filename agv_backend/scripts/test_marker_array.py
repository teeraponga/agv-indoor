#!/usr/bin/env python3

"""
read list of position from yaml and publish as markerArray topic
"""

from os import read
import yaml
from yaml.error import Mark
from std_msgs.msg import String
from visualization_msgs.msg import Marker, MarkerArray
import tf
import rospy
import rospkg
import random

from copy import deepcopy
from agv_backend.srv import marker_array

class test_marker_array:
    def __init__(self) -> None:
        self.frame_id = "map"
        self.marker_ns = "saved_positions"
        self.marker_array_text_msg = MarkerArray()
        self.marker_array_shape_msg = MarkerArray()
        self.pub = rospy.Publisher("visualization_marker_array",MarkerArray, queue_size=10)
        self.pub2 = rospy.Publisher("visualization_marker_array_shape",MarkerArray, queue_size=10)
        self.service = rospy.Service('get_marker', marker_array, self.service_callback)

        self.saved_positions = {}
        # self.read_yaml()
        # self.prepare_msg()
        pass

    def read_yaml(self):
        pkg = rospkg.RosPack()
        path = "{}/saved_positions/test.yaml".format(pkg.get_path('agv_navigation'))
        with open(path, "r") as stream:
            try:
                self.saved_positions = yaml.safe_load(stream)
            except yaml.YAMLError as exc:
                print(exc)

    def prepare_msg(self):
        self.marker_array_text_msg = MarkerArray()
        for index, key in enumerate(self.saved_positions.keys()):
            msg = Marker()
            msg.ns = self.marker_ns
            msg.id = index + 1
            msg.header.stamp = rospy.Time.now()
            msg.header.frame_id = self.frame_id
            msg.color.a = 1.0
            msg.lifetime.secs = 0
            msg.type = 9
            msg.scale.x = 0.5
            msg.scale.y = 0.5
            msg.scale.z = 0.5
            msg.pose.position.x = self.saved_positions[key]['px']
            msg.pose.position.y = self.saved_positions[key]['py']
            msg.pose.position.z = 0.5
            msg.pose.orientation.z = self.saved_positions[key]['qz']
            msg.pose.orientation.w = self.saved_positions[key]['qw']
            # msg.color.r = random.randrange(0,255)
            # msg.color.g = random.randrange(0,255)
            # msg.color.b = random.randrange(0,255)
            msg.color.r = random.random()
            msg.color.g = random.random()
            msg.color.b = random.random()
            msg.text = key
            self.marker_array_text_msg.markers.append(msg)
            msg2 = deepcopy(msg)
            msg2.id = -1 * msg2.id
            msg2.type = 2
            msg2.scale.x = 0.2
            msg2.scale.y = 0.2
            msg2.scale.z = 0.2
            msg2.pose.position.z = 0.0
            self.marker_array_shape_msg.markers.append(msg2)
            
        pass

    def service_callback(self, req):
        # print("aasd")
        self.read_yaml()
        self.prepare_msg()
        self.pub.publish(self.marker_array_text_msg)
        self.pub2.publish(self.marker_array_shape_msg)
        return []

if(__name__=="__main__"):
    rospy.init_node("test_marker_array")
    test_marker_array()
    rospy.spin()

