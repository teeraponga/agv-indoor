#!/usr/bin/env python3

import rospy
import time
from std_msgs.msg import String, Bool

"""
Change map process
1. run agv_backend/process_watcher.py
2. set ros param "map" to name of map.
3. if other navigation is running, publish "navigation_stop" to topic /command
4. publish "navigation_start" to topic /command.
5. process_watcher.py will read rosparam "map" and start navigation
"""

first_map = "room12"
second_map = "room12_corridor"

wait_nav_shutdown = 10

change = False
def callback(data):
    global change
    if(data.data):
        change = True

if(__name__=="__main__"):
    rospy.init_node("test_change_map")
    print("set publisher")
    pub = rospy.Publisher("/command", String, queue_size=10)
    sub = rospy.Subscriber("/change", Bool, callback)
    time.sleep(5)

    print("set map name")
    rospy.set_param("map", first_map)
    print("start navigation")
    pub.publish(String("navigation_start"))

    ### wait as if robot is in use
    print("navigation working")
    
    while(not change):
        pass

    ### try to switch map
    print("stop navigation")
    pub.publish(String("navigation_stop"))
    time.sleep(10)
    print("set map name")
    rospy.set_param("map", second_map)
    print("start navigation")
    pub.publish(String("navigation_start"))
