#!/usr/bin/env python3

import rospy
import yaml
import time

from get_path import App
from point_navigation import Point_navigation
from geometry_msgs.msg import Pose, PoseStamped, PoseWithCovarianceStamped
from std_msgs.msg import Bool, String

"""
on start:
    1. set rosparam map to default map
    2. start navigation with topic /command
on idle(task finished, waiting for goal, etc.):
    - update robot position in map
    - keep listening for command e.g. goal topic.
on receiving goal:
    1. get all transition points in current map.
    ### FIND START AND GOAL NODE ###
    2. find nearest transition points to current robot position and use it as start node 
        TODO: ****need better algorithm for finding start node****
    3. repeat step 1 and 2 for goal map/position to get goal node
    ### NEO4J SHORTEST PATH ###
    4. get shortest path. will get result as node list and relationship list
    ### NAVIGATE AND TRANSITION FROM MAP TO MAP ###
    5. use goal from node ist navigate until find transition to next map
    6. use transition action (e.g. lift, door) to go to next map
    7. change map (set rosparam, then restart navigation with topic /command) then set initial position
    8. repeat 5-7 unil reach goal

***main function is workflow()***
"""

class multimap_navigation_manager:
    def __init__(self, uri, user, password) :
        self.point_navigate = Point_navigation()
        self.app = App(uri, user, password)
        self.nodes = []
        self.relationships = []

        self.command_pub = rospy.Publisher("/command", String, queue_size=10)

        # self.map = "hotel_kitchen"
        self.map = "room12_1"
        self.navigation_started = False

        self.cancel_sub = rospy.Subscriber("/workflow_cancel", Bool, self.cancel_callback)

        self.goal_pub = rospy.Publisher("/move_base_simple/goal", PoseStamped, queue_size=10)
        self.goal_sub = rospy.Subscriber("/multimap/goal", PoseStamped, self.goal_callback)

        self.last_goal_pose = Pose()
        self.last_goal_map = ""
        self.robot_pose = Pose()
        self.robot_pose_sub = rospy.Subscriber("/robot_pose", Pose, self.robot_pose_callback)

        self.nodes = []
        self.relationships = []
        self.start_workflow = False
        self.workflow_cancel = False

        ### transition ###
        self.transition_sub = rospy.Subscriber("/transition", Bool, self.mock_transition)
        self.trans_finish = False

        ### set position in map
        self.set_pose_pub = rospy.Publisher("/initialpose",PoseWithCovarianceStamped, queue_size=10)
        self.set_pose_msg = PoseWithCovarianceStamped()
        self.set_pose_msg.header.frame_id = "map"
        self.set_pose_msg.pose.covariance = [0.25, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
                                             0.25, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
                                             0.0,  0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
                                             0.0,  0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
                                             0.0,  0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
                                             0.06853892326654787]

    def goal_callback(self, goal=PoseStamped()):
        """     Wait for goal message
        1. Find start and goal node in database.
        2. Get shortest path.
        3. Set flag start workflow.
        """
        ### Get map name and pose ###
        start_map = self.map
        start_pose = self.robot_pose
        self.last_goal_map = goal.header.frame_id
        self.last_goal_pose = goal.pose
        ### Get start/goal node ###
        id_in_start_map = self.get_nearest_transition(map_name=start_map, pose=start_pose)
        id_in_goal_map = self.get_nearest_transition(map_name=self.last_goal_map, pose=self.last_goal_pose)
        ### Get shortest path node/relationship list ###
        self.nodes, self.relationships = self.get_path(start_map=start_map, start_id=id_in_start_map, goal_map=self.last_goal_map, goal_id=id_in_goal_map)
        print(len(self.nodes))
        print(len(self.relationships))
        self.start_workflow = True

    def get_nearest_transition(self, map_name="", pose=Pose()):

        """id = -1

        ### Load transition file ###
        pass
        ### Loop transition points and find nearest ###
        pass

        return id"""
        return 0

    def robot_pose_callback(self,pose=Pose()):
        ### Update robot's current position
        self.robot_pose = pose

    def cancel_callback(self, data):
        print("CANCELED: {}".format(data.data))
        self.workflow_cancel = data.data

    def get_path(self, start_map, start_id, goal_map, goal_id):
        print("get path from {0}_{1} to {2}_{3}".format(start_map, start_id, goal_map, goal_id))
        nodes, relationships = self.app.get_shortest_path(start_map, start_id, goal_map, goal_id)
        return nodes, relationships
    
    def mock_transition(self, data):
        self.trans_finish = data.data

    def change_map(self, map_name="", pose=Pose()):
        """  Process
        1. Stop current navigation session.
        2. Set rosparam "/map" to map's name.
        3. Start navigation session.
        4. If pose is given, set robot's initial pose.
        """
        print("Terminating navigation....")
        self.command_pub.publish(String("navigation_stop"))
        self.navigation_started = False
        self.map = map_name
        rospy.set_param("map", map_name)
        time.sleep(5)
        print("Set map to {}".format(map_name))
        print("Starting navigation....")
        self.command_pub.publish(String("navigation_start"))
        time.sleep(5)
        self.set_pose_in_map(pose=pose)
        time.sleep(2)
        self.navigation_started = True
        print("Navigation Started!!")

    def set_pose_in_map(self,pose=Pose()):
        self.set_pose_msg.header.stamp = rospy.Time.now()
        self.set_pose_msg.pose.pose = pose
        self.set_pose_pub.publish(self.set_pose_msg)

    def call_transition(self):
        pass

    def workflow(self):
        for index, node in enumerate( self.nodes):
            # if(node["type"]!="position"):
            #     continue
            ### Change Map if not in same map as node ###
            if(self.map!=node["at_map"]):
                print("Changeing map...")
                pose = Pose()
                pose.position.x = node["px"]
                pose.position.y = node["py"]
                pose.orientation.z = node["qw"]
                pose.orientation.w = -1* node["qz"]
                self.change_map(node["at_map"], pose)

            ### Set transition finish to false ###
            self.trans_finish = False
            
            ### Create goal message from node ###
            goal = PoseStamped()
            goal.header.stamp = rospy.Time.now()
            goal.header.frame_id = "map"
            goal.pose.position.x = node["px"]
            goal.pose.position.y = node["py"]
            goal.pose.orientation.z = node["qz"]
            goal.pose.orientation.w = node["qw"]
            
            ### Navigate to next node ###
            self.point_navigate.send_goal(goal=goal)
            print("Sending Goal message...")
            while(not self.point_navigate.goal_sent and not self.workflow_cancel):
                pass
            print("Goal sent, waiting for navigation to finish...")
            while(not self.point_navigate.goal_reached and not self.workflow_cancel):
                ### wait for current goal to finish
                pass
            if(index==len(self.nodes)-1):
                print("Last map reached, proceed to last goal.")
                break

            ### if not last node, trigger transition action ###
            ### no transition if node are on same map ###
            if(not self.relationships[index]["relate_type"]=="SAME_MAP"):
                print("waiting for transition...")
                self.call_transition()
                while(not self.trans_finish and not self.workflow_cancel):
                    pass
                print("finished transition")
            
        ### After traversed all nodes, go to last goal ###
        goal = PoseStamped()
        goal.header.stamp = rospy.Time.now()
        goal.header.frame_id = "map"
        goal.pose = self.last_goal_pose
        self.point_navigate.send_goal(goal=goal)
        print("Sending Last Goal message...")
        while(not self.point_navigate.goal_sent and not self.workflow_cancel):
            pass
        print("Last Goal sent, waiting for navigation to finish...")
        while(not self.point_navigate.goal_reached and not self.workflow_cancel):
            ### wait for current goal to finish
            pass
        print("Last goal reached.")
        print("Terminating workflow.")
            

if(__name__=="__main__"):
    uri = "neo4j+s://05c3b0e2.databases.neo4j.io"
    user = "neo4j"
    password = "K9JQB9KTNDYLRENAt8IoM2LEcSKQctkirZq2UJkGfwE"

    rospy.init_node("multimap_navigation")

    mulnav = multimap_navigation_manager(uri=uri, user=user, password=password)

    print("starting first navigation with map {}".format(mulnav.map))
    rospy.set_param("map", mulnav.map)
    time.sleep(0.5)
    mulnav.command_pub.publish(String("navigation_start"))
    print("navigation started..")
    try:
        while(not rospy.is_shutdown()):
            if(mulnav.start_workflow):
                mulnav.workflow()
                mulnav.start_workflow = False
    except KeyboardInterrupt:
        mulnav.command_pub.publish(String("navigation_stop"))
        mulnav.app.close()
        print("TERMINATED BY USER!!!")
        time.sleep(1)