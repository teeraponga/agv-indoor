#!/usr/bin/env python3

import os
import glob
import rospkg
import yaml

# from node import node

from neo4j import GraphDatabase
import logging
from neo4j.exceptions import ServiceUnavailable


"""
Read all transition.yaml files from all map folders.
Generate into transition graph that link all maps together.
Save graph for later use.
"""

class App:

    def __init__(self, uri, user, password):
        self.driver = GraphDatabase.driver(uri, auth=(user, password))

    def close(self):
        # Don't forget to close the driver connection when you are finished with it
        self.driver.close()

    ### POSITION NODE ### 
    def create_position_node(self, id_in_map, at_map, px, py, qz, qw, position_type="TRANSITION"):
        with self.driver.session() as session:
            # Write transactions allow the driver to handle retries and transient errors
            result = session.write_transaction(
                self._create_position_node, id_in_map, at_map, px, py, qz, qw, position_type="TRANSITION")

    @staticmethod
    def _create_position_node(tx, id_in_map, at_map, px, py, qz, qw, position_type="TRANSITION"):
        query = (
            "CREATE (p:position { "
            "id_in_map: $id_in_map,"
            "at_map: $at_map,"
            "px: $px,"
            "py: $py,"
            "qz: $qz,"
            "qw: $qw,"
            "position_type: $position_type"
            "}) "
        )
        result = tx.run(query, id_in_map=id_in_map, at_map=at_map, px=px, py=py, qz=qz, qw=qw, position_type=position_type )
        return result

    ### SAME_MAP relationship ###
    def create_same_map_relationship(self):
        with self.driver.session() as session:
            result = session.read_transaction(self._create_same_map_relationship)

    @staticmethod
    def _create_same_map_relationship(tx):
        query = (
            "MATCH (n:position),(m:position) "
            "WHERE n.at_map=m.at_map and n.id_in_map<>m.id_in_map "
            "CREATE (n)-[:SAME_MAP{"
            "    distance: sqrt((m.px-n.px)^2+(m.py-n.py)^2)"
            "}]->(m)"
        )
        result = tx.run(query)
        return result


    ### TRANSITION relationship ###
    """
    MATCH (u:position {id:$id_1, at_map:$map_1}), (r:position {id:$id_2, at_map:$map_2})
    CREATE (u)-[:TRANSITION {cost:$cost, distance:$distance, type:$type}]->(r)
    """

    def create_transition_relationship(self, id_1, map_1, id_2, map_2, cost, distance, type):
        with self.driver.session() as session:
            result = session.read_transaction(self._create_transition_relationship, id_1, map_1, id_2, map_2, cost, distance, type)

    @staticmethod
    def _create_transition_relationship(tx, id_1, map_1, id_2, map_2, cost, distance, type):
        query = (
            "MATCH (u:position { id_in_map:$id_1, at_map:$map_1}), (r:position {id_in_map:$id_2, at_map:$map_2})"
            "CREATE (u)-[:TRANSITION { cost:$cost, distance:$distance, type:$type}]->(r)"
        )
        result = tx.run(query, id_1=id_1, map_1=map_1, id_2=id_2, map_2=map_2, cost=cost, distance=distance, type=type)

    ### ELEVATOR NODE AND RELATIONSHIP ### 
    ## NODE ##
    

class transition_graph_generator:
    def __init__(self) -> None:
        rp = rospkg.RosPack()

        ### READ CONFIG FILE ### 
        config_file = "{}/config/config.yaml".format(rp.get_path('multi_map_planner'))
        try:
            with open(config_file) as f:
                self.config = yaml.safe_load(f)
        except Exception as e:
            print(e)

        ### GET MAP FOLDER PATH ###
        """
        MAPS folder will be in this format
        maps/
            transition_relationship.yaml    ***File that contain relationship of all transition points***
            <map name 1>/
                map.pbstream                Pose graph file for cartographer
                map.pgm                     Map's image
                map.yaml                    Map's metadata
                transition.yaml             ***File that contain all transition point(s) in map***
            <map name 2>/
                                .
                                .
                                .
        """
        self.maps_path = "{0}/{1}".format(rp.get_path(self.config['map_package']), self.config['map_folder'])

        ## neo4j object ###
        self.uri = "neo4j+s://05c3b0e2.databases.neo4j.io"
        self.user = "neo4j"
        self.password = "K9JQB9KTNDYLRENAt8IoM2LEcSKQctkirZq2UJkGfwE"
        self.app = App(self.uri, self.user, self.password)

    
    def init_node(self):
        map_path_list = glob.glob('{0}/*'.format(self.maps_path))
        map_name_list = [os.path.basename(x) for x in map_path_list]
        ### Get list of map name. Then iterate into each map folder for transition.yaml
        for map in map_name_list:
            if(map=="transition_relationship.yaml"):
                continue
            transition_path = "{0}/{1}/transition.yaml".format(self.maps_path, map)
            with open(transition_path, 'r') as file:
                trans = yaml.safe_load(file)
                file.close()

            ### Create position nodes ###
            for tran in trans:
                px = trans[tran]['pos']['px']
                py = trans[tran]['pos']['py']
                qz = trans[tran]['pos']['qz']
                qw = trans[tran]['pos']['qw']
                self.app.create_position_node(id_in_map=tran,at_map=map,px=px,py=py,qz=qz,qw=qw)
            
            ### Create SAME_MAP relationship ###
        self.app.create_same_map_relationship()



    def init_transition_relationship(self):
        relationship_path = "{0}/transition_relationship.yaml".format(self.maps_path)
        with open(relationship_path, 'r') as file:
            relationships = yaml.safe_load(file)
        for rel in relationships:
            type = relationships[rel]['type']
            cost = relationships[rel]['cost']
            distance = relationships[rel]['distance']
            locations = relationships[rel]['locations']
            while(len(locations)>1):
                tmp = locations.pop(0)
                map_1 = tmp['map']
                id_1 = tmp['id']
                for location in locations:
                    map_2 = location['map']
                    id_2 = location['id']
                    self.app.create_transition_relationship(id_1, map_1, id_2, map_2, cost, distance, type)



    def init_graph(self):
        self.init_node()
        self.init_transition_relationship()
        self.app.close()

# if(__name__=="__main__"):
#     uri = "neo4j+s://05c3b0e2.databases.neo4j.io"
#     user = "neo4j"
#     password = "K9JQB9KTNDYLRENAt8IoM2LEcSKQctkirZq2UJkGfwE"
#     app = App(uri, user, password)
#     app.create_position_node(0,"map",0,0,0,1)
#     app.create_position_node(0,"map2",0,0,0,1)
#     app.create_transition_relationship(0, "map", 0, "map2", 0.5, 1, "door")
#     app.close()

if(__name__=="__main__"):
    generator = transition_graph_generator()
    generator.init_graph()