#!/usr/bin/env python3

import rospy
import time

from get_path import App
from point_navigation import Point_navigation
from geometry_msgs.msg import Pose, PoseStamped, PoseWithCovarianceStamped
from std_msgs.msg import Bool, String
import math

import roslib
roslib.load_manifest('transition')
from transition.msg import TransitionAction, TransitionGoal, TransitionResult, TransitionFeedback
import actionlib

"""
on start:
    1. set rosparam map to default map
    2. start navigation with topic /command
on idle(task finished, waiting for goal, etc.):
    - update robot position in map
    - keep listening for command e.g. goal topic.
on receiving goal:
    1. get all transition points in current map.
    ### FIND START AND GOAL NODE ###
    2. find nearest transition points to current robot position and use it as start node 
        TODO: ****need better algorithm for finding start node****
    3. repeat step 1 and 2 for goal map/position to get goal node
    ### NEO4J SHORTEST PATH ###
    4. get shortest path. will get result as node list and relationship list
    ### NAVIGATE AND TRANSITION FROM MAP TO MAP ###
    5. use goal from node ist navigate until find transition to next map
    6. use transition action (e.g. lift, door) to go to next map
    7. change map (set rosparam, then restart navigation with topic /command) then set initial position
    8. repeat 5-7 unil reach goal

***main function is workflow()***
"""

class multimap_navigation_manager:
    def __init__(self, uri, user, password) :
        self.point_navigate = Point_navigation()
        self.app = App(uri, user, password)
        self.nodes = []
        self.relationships = []

        self.command_pub = rospy.Publisher("/command", String, queue_size=10)

        self.map = "hotel_main"
        # self.map = "floor1"
        self.navigation_started = False

        self.cancel_sub = rospy.Subscriber("/workflow_cancel", Bool, self.cancel_callback)

        self.goal_pub = rospy.Publisher("/move_base_simple/goal", PoseStamped, queue_size=10)
        self.goal_sub = rospy.Subscriber("/multimap/goal", PoseStamped, self.goal_callback)

        self.last_goal_pose = Pose()
        self.last_goal_map = ""
        self.robot_pose = Pose()
        self.robot_pose_sub = rospy.Subscriber("/robot_pose", Pose, self.robot_pose_callback)

        self.nodes = []
        self.relationships = []
        self.start_workflow = False
        self.workflow_cancel = False

        ### transition ###
        self.transition_goal = TransitionGoal()
        self.client = actionlib.SimpleActionClient('Transition', TransitionAction)
        self.transition_sub = rospy.Subscriber("/transition", Bool, self.mock_transition)
        self.trans_finish = False

        ### set position in map
        self.set_pose_pub = rospy.Publisher("/initialpose",PoseWithCovarianceStamped, queue_size=10)
        self.set_pose_msg = PoseWithCovarianceStamped()
        self.set_pose_msg.header.frame_id = "map"
        self.set_pose_msg.pose.covariance = [0.25, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
                                             0.25, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
                                             0.0,  0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
                                             0.0,  0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
                                             0.0,  0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
                                             0.06853892326654787]

    def goal_callback(self, goal=PoseStamped()):
        """     Wait for goal message
        1. Find start and goal node in database.
        2. Get shortest path.
        3. Set flag start workflow.
        """
        if(not self.start_workflow):
            ### Get map name and pose ###
            start_map = self.map
            start_pose = self.robot_pose
            self.last_goal_map = goal.header.frame_id
            self.last_goal_pose = goal.pose
            ### Get start/goal node ###
            id_in_start_map = self.get_nearest_position(map_name=start_map, pose=start_pose)
            id_in_goal_map = self.get_nearest_position(map_name=self.last_goal_map, pose=self.last_goal_pose)
            ### Get shortest path node/relationship list ###
            self.nodes, self.relationships = self.get_path(start_map=start_map, start_id=id_in_start_map, goal_map=self.last_goal_map, goal_id=id_in_goal_map)
            print(len(self.nodes))
            print(len(self.relationships))
            self.start_workflow = True
        else:
            print("Robot is working, Reject goal.")

    def get_nearest_position(self, map_name="", pose=Pose()):
        """
        Get id of node nearest to input pose to use as start/goal node.
        """
        id = -1
        min_dist = math.inf

        ### Get all position nodes in map ###
        nodes = self.app.get_all_position_nodes_in_map(map=map_name)
        ### Loop in all nodes and find nearest node ###
        x1 = pose.position.x
        y1 = pose.position.y
        for node in nodes:
            print(node)
            x2 = node["px"]
            y2 = node["py"]
            dist = self.calculate_distance(x1,y1,x2,y2)
            if(min_dist > dist):
                id = node["id_in_map"]
                min_dist = dist
        return id

    def calculate_distance(self, x1,y1,x2,y2):
        return math.sqrt( pow(x1-x2,2) + pow(y1-y2,2) )

    def robot_pose_callback(self,pose=Pose()):
        ### Update robot's current position
        self.robot_pose = pose

    def cancel_callback(self, data):
        print("CANCELED: {}".format(data.data))
        self.workflow_cancel = data.data

    def get_path(self, start_map, start_id, goal_map, goal_id):
        print("get path from {0}_{1} to {2}_{3}".format(start_map, start_id, goal_map, goal_id))
        nodes, relationships = self.app.get_shortest_path(start_map, start_id, goal_map, goal_id)
        return nodes, relationships

    def change_map(self, map_name="", pose=Pose()):
        """  Process
        1. Stop current navigation session.
        2. Set rosparam "/map" to map's name.
        3. Start navigation session.
        4. If pose is given, set robot's initial pose.
        """
        print("Change map, terminating current navigation....")
        self.command_pub.publish(String("navigation_stop"))
        self.navigation_started = False
        self.map = map_name
        rospy.set_param("map", map_name)
        time.sleep(5)
        print("Set map to {}".format(map_name))
        print("Starting navigation....")
        self.command_pub.publish(String("navigation_start"))
        time.sleep(5)
        self.set_pose_in_map(pose=pose)
        time.sleep(2)
        self.navigation_started = True
        print("Navigation Started!!")

    def set_pose_in_map(self,pose=Pose()):
        self.set_pose_msg.header.stamp = rospy.Time.now()
        self.set_pose_msg.pose.pose = pose
        self.set_pose_pub.publish(self.set_pose_msg)
    
    def mock_transition(self, data):
        self.trans_finish = data.data
    
    def feedback_transition(self, data=TransitionFeedback()):
        print("Transition at: {}%".format(data.percent_complete))
        if(data.percent_complete==100):
            self.trans_finish = True

    def result_transition(self, status, data=TransitionResult()):
        if(data.transition_result=="1" or status==3):
            self.trans_finish = True
        else:
            self.trans_finish = False
            print(data.message)
        pass

    def call_transition(self, relationship_before, node, relationship_after):
        self.transition_goal.current_floor = relationship_before["floor"]
        self.transition_goal.target_floor = relationship_after["floor"]
        self.transition_goal.distance = relationship_before["distance"]
        self.transition_goal.wait_time = relationship_before["wait_time"]
        self.transition_goal.type = node["type_name"]
        self.client.send_goal(self.transition_goal, done_cb=self.result_transition, feedback_cb=self.feedback_transition)
        pass

    def workflow(self):
        for index, node in enumerate( self.nodes):
            ### If canceled, break out from loop ###
            if(self.workflow_cancel):
                break
            ### Group node, skip to next node ###
            if(node["type"]=="group"):
                continue
            ### Position node, either trigger change map or navigation 
            elif(node["type"]=="position"):
                ### Change Map if not in same map as previouse node ###
                if(self.map!=node["at_map"]):
                    print("Changeing map...")
                    pose = Pose()
                    pose.position.x = node["px"]
                    pose.position.y = node["py"]
                    pose.orientation.z = node["qw"]
                    pose.orientation.w = -1* node["qz"]
                    self.change_map(node["at_map"], pose)
                    ### skip navigation process because robot will be at this node's position after finished transition anyway
                    continue 

                ### Set transition finish to false ###
                self.trans_finish = False
            
                ### Create goal message from node ###
                goal = PoseStamped()
                goal.header.stamp = rospy.Time.now()
                goal.header.frame_id = "map"
                goal.pose.position.x = node["px"]
                goal.pose.position.y = node["py"]
                goal.pose.orientation.z = node["qz"]
                goal.pose.orientation.w = node["qw"]
            
                ### Set desired fallback behavior ###
                if(index == len(self.relationships)):
                    self.point_navigate.set_fallback_mode(self.point_navigate.TOLERATE_DISTANCE, tolerable_distance=1.0)
                elif(self.relationships[index]["relate_type"]=="TRANSITION"):
                    self.point_navigate.set_fallback_mode(self.point_navigate.RESEND_GOAL)
                elif(self.relationships[index]["relate_type"]=="IN_GROUP"):
                    self.point_navigate.set_fallback_mode(self.point_navigate.TOLERATE_DISTANCE, tolerable_distance=1.0)
                ### Navigate to node ###
                self.point_navigate.send_goal(goal=goal)
                print("Sending Goal message...")
                while(not self.point_navigate.goal_sent and not self.workflow_cancel):
                    ### Add time.sleep to prevent infinite loop to consume all CPU
                    time.sleep(0.5)
                    pass
                print("Goal sent, waiting for navigation to finish...")
                while(not self.point_navigate.goal_reached and not self.workflow_cancel):
                    ### wait for current goal to finish
                    time.sleep(0.5)
                    pass
                ### Last node in path will always be position node, so set exit condition here
                if(index==len(self.nodes)-1):
                    print("Last map reached, proceed to last goal.")
                    break
            
            #####################################
            ### Transition node, trigger transition behavior. send node data and relationship before and after this node
            #####################################
            elif(node["type"]=="transition"):
                print("waiting for transition...")
                ## Transition node data is in variable name "node"
                rel_before = self.relationships[index-1]
                rel_after = self.relationships[index]
                self.call_transition(relationship_before=rel_before, node=node, relationship_after=rel_after)
                while(not self.trans_finish and not self.workflow_cancel):
                    time.sleep(0.5)
                    pass
                print("finished transition")
            
        ### After traversed all nodes, go to last goal ###
        if(not self.workflow_cancel):
            goal = PoseStamped()
            goal.header.stamp = rospy.Time.now()
            goal.header.frame_id = "map"
            goal.pose = self.last_goal_pose
            self.point_navigate.set_fallback_mode(self.point_navigate.RESEND_GOAL)
            self.point_navigate.send_goal(goal=goal)
            print("Sending Last Goal message...")
            while(not self.point_navigate.goal_sent and not self.workflow_cancel):
                time.sleep(0.5)
                pass
            print("Last Goal sent, waiting for navigation to finish...")
            while(not self.point_navigate.goal_reached and not self.workflow_cancel):
                ### wait for current goal to finish
                time.sleep(0.5)
                pass
            print("Last goal reached.")
        print("Terminating workflow.")
            

if(__name__=="__main__"):
    # uri = "neo4j+s://05c3b0e2.databases.neo4j.io"
    # user = "neo4j"
    # password = "K9JQB9KTNDYLRENAt8IoM2LEcSKQctkirZq2UJkGfwE"

    uri = "bolt://localhost:11003"
    # uri = "bolt://localhost:7687"
    user = "neo4j"
    password = "12345678"

    rospy.init_node("multimap_navigation")

    mulnav = multimap_navigation_manager(uri=uri, user=user, password=password)

    print("starting first navigation with map {}".format(mulnav.map))
    rospy.set_param("map", mulnav.map)
    time.sleep(0.5)
    mulnav.command_pub.publish(String("navigation_start"))
    print("navigation started..")
    try:
        while(not rospy.is_shutdown()):
            if(mulnav.start_workflow and not mulnav.workflow_cancel):
                mulnav.workflow()
                mulnav.start_workflow = False
    except KeyboardInterrupt:
        mulnav.command_pub.publish(String("navigation_stop"))
        mulnav.app.close()
        print("TERMINATED BY USER!!!")
        time.sleep(1)