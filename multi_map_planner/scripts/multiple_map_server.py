#!/usr/bin/env python3

import rospy
import roslaunch
import rospkg

import glob
import os

if(__name__=="__main__"):
    rospy.init_node("multiple_map_server")
    pkg = rospkg.RosPack()

    launch_pkg_path = pkg.get_path("multi_map_planner")

    maps_pkg_path = pkg.get_path("agv_slam")
    maps_path = "{0}/{1}".format(maps_pkg_path, "maps")

    map_path_list = glob.glob('{0}/**'.format(maps_path))
    map_name_list = [os.path.basename(x) for x in map_path_list]

    launch_file = "map_server_with_arg.launch"

    launch_list = []
    for index, map in enumerate( map_name_list):
        if(map=="transition_relationship.yaml"):
            continue
        uuid = roslaunch.rlutil.get_or_generate_uuid(None, False)
        tmp = roslaunch.parent.ROSLaunchParent(uuid, 
        [(
            "{0}/launch/{1}".format(launch_pkg_path, launch_file),
            [
                "map_name:={0}".format(map),
                "map_ns:={0}".format(map)
            ]
        )])
        launch_list.append(tmp)

    for index, launch in enumerate( launch_list):
        print("start map {}".format(index))
        launch.start()
        
    try:
        rospy.spin()
    except KeyboardInterrupt:
        for launch in launch_list:
            launch.shutdown()
        print("terminated")
        pass