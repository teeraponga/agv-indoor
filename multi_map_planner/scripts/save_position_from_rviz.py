#!/usr/bin/env python3
import rospy
import yaml

from geometry_msgs.msg import PoseWithCovarianceStamped

n = 0

filename = "transition.yaml"

# open file 
f = open(filename, "a") 
  
# absolute file positioning
f.seek(0) 
  
# to erase all data 
f.truncate() 

def init_callback(data):
    global n
    print("mark!")
    dict_1 = {
        n:{ 
            'pos':{
                'px':data.pose.pose.position.x,
                'py':data.pose.pose.position.y,
                'qz':data.pose.pose.orientation.z,
                'qw':data.pose.pose.orientation.w
            },
            "name": ""
        }
    }    
    # new_dict = {
    #     'pos_2': {
    #         'x':data.pose.pose.position.x,
    #         'y':data.pose.pose.position.y,
    #         'qz':data.pose.pose.orientation.z,
    #         'qw':data.pose.pose.orientation.w
    #     }
    # }
    
    # with open('/home/cash/catkin_ws/src/Read_yaml/scripts/location.yaml', 'r') as f:
    #     dict_1 = yaml.safe_load(f)
    #     dict_1.update(new_dict)

    n += 1  
    print(n) 
    with open(filename, 'a') as f:
        output = yaml.dump(dict_1, f)


# rospy.Subscriber(<TOPIC NAME>, <MESSAGE TYPE>, <CALLBACK FUNCTION>)
rospy.init_node("yaml")
sub = rospy.Subscriber("/initialpose",PoseWithCovarianceStamped,init_callback)


rospy.spin()