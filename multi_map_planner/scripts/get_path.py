#!/usr/bin/env python3

from neo4j import GraphDatabase
import logging
from neo4j.exceptions import ServiceUnavailable

"""
Search shortest path from node A to node B
input: map name and id of node A and B
output: list of Node and Relationship between A and B (node list Include A and B)

---------------------------------------------------------------------------
Node structure
--POSITION node
    { 
        "at_map": String - map's name, 
        "id_in_map": int - ID of node,
        "px": float - position,
        "py": float - position,
        "qz": float - quaternion orientatioin,
        "qw": float - quaternion orientatioin,
        "type": "position" - String of node type name
    }

--GROUP node
    {
        "type": "group" - String of node type name
    }

--TRANSITION node
    {
        "type": "transition" - String of node type name
        "type_id": Int - Transition type ID 
        "type_name": String - Transition type name
        "trans_id": int - transition node ID
    }
---------------------------------------------------------------------------
Relationship structure
--IN_GROUP relationship
{
    "relate_type": "IN_GROUP" - String of relationship type
}

--TRANSITION relationship
{
    "relate_type": "TRANSITION" - String of relationship type
    "floor": String - floor number
    "cost": float - cost of transition
    "distance": float - distance of transition that robot need to walk
    "wait_time": float - time robot need to wait
}
---------------------------------------------------------------------------
"""

class App:

    def __init__(self, uri, user, password):
        self.driver = GraphDatabase.driver(uri, auth=(user, password))

    def close(self):
        # Don't forget to close the driver connection when you are finished with it
        self.driver.close()

    ########################################
    ### GET ALL POSITION NODE FROM GRAPH ###

    def get_all_position_node(self):
        with self.driver.session() as session:
            result = session.read_transaction(self._get_all_position_node)
            return result

    @staticmethod
    def _get_all_position_node(tx):
        query = (
            "MATCH (p:position) "
            "RETURN p"
        )
        result = tx.run(query)
        return [{"map": row["p"]["at_map"], "id_in_map": row["p"]["id_in_map"]}
                    for row in result]

    ##################################
    ### GET SPECIFIC POSITION NODE ###

    def get_position_node(self, map, id):
        with self.driver.session() as session:
            result = session.read_transaction(self._get_position_node, map, id)
            return result

    @staticmethod
    def _get_position_node(tx, map="", id=""):
        query = (
            "MATCH (p:position) "
            "where p.at_map=$map and p.id_in_map=$id"
            "RETURN p"
        )
        result = tx.run(query, map=map, id=id)
        return [{"map": row["p"]["at_map"], "id_in_map": row["p"]["id_in_map"]}
                    for row in result]

    ##################################
    ### GET ALL POSITION NODE IN SPECIFIC MAP ###

    def get_all_position_nodes_in_map(self, map):
        with self.driver.session() as session:
            result = session.read_transaction(self._get_all_position_nodes_in_map, map)
            return result

    @staticmethod
    def _get_all_position_nodes_in_map(tx, map=""):
        query = (
            "MATCH (p:position) "
            "where p.at_map=$map "
            "RETURN p"
        )
        result = tx.run(query, map=map)
        return [{"map": row["p"]["at_map"], "id_in_map": row["p"]["id_in_map"], "name":row["p"]["name"],
                "px": row["p"]["px"],"py": row["p"]["py"],"qz": row["p"]["qz"],"qw": row["p"]["qw"]}
                    for row in result]

    ##################################
    ### GET SHORTEST PATH FROM POSITION NODE A TO B ###

    def get_shortest_path(self, map_1, id_1, map_2, id_2):
        with self.driver.session() as session:
            nodes, relationships = session.read_transaction(self._get_shortest_path, map_1, id_1, map_2, id_2)
            return nodes, relationships

    """
    Use neo4j search for shortest path.
    input: map name and id in map of start and goal position
    return: list of nodes and relationships from start to goal. in order
        
        list node: n,   list relationship: r

            n[0](start)  --r[0]-->   n[1]  --r[1]-->  . . . . . --r[]--> n[](goal)

        ** len(r) = len(n)-1 **
    """
    @staticmethod
    def _get_shortest_path(tx, map_1, id_1, map_2, id_2):
        query = (
            "MATCH (n:position { at_map: $map_1, id_in_map: $id_1} ),"
            "    (m:position { at_map: $map_2, id_in_map: $id_2}),"
            "    path = shortestPath((n)-[*]-(m))"
            "RETURN path"
        )
        path = tx.run(query, map_1=map_1, id_1=id_1, map_2=map_2, id_2=id_2)
        
        nodes_list = []
        relationships_list = []

        for record in path:
            nodes = record["path"].nodes
            relationships = record["path"].relationships
            for node in nodes:
                if(list(node.labels)[0]=="position"):
                    nodes_list.append({ "at_map": node["at_map"], 
                                        "id_in_map": node["id_in_map"],
                                        "px": node["px"],
                                        "py": node["py"],
                                        "qz": node["qz"],
                                        "qw": node["qw"],
                                        "type": list(node.labels)[0]
                                    })
                elif(list(node.labels)[0]=="group"):
                    nodes_list.append({"type": list(node.labels)[0]})
                elif(list(node.labels)[0]=="transition"):
                    nodes_list.append({ "type": list(node.labels)[0],
                                        "type_id": node["type_id"],
                                        "type_name": node["type_name"],
                                        "trans_id": node["trans_id"] 
                                    })

            # nodes_list = [{ "at_map": node["at_map"], 
            #                 "id_in_map": node["id_in_map"],
            #                 "px": node["px"],
            #                 "py": node["py"],
            #                 "qz": node["qz"],
            #                 "qw": node["qw"],
            #                 } for node in nodes]

            for relationship in relationships:
                if(relationship.type=="IN_GROUP"):
                    relationships_list.append({"relate_type": relationship.type})
                elif(relationship.type=="TRANSITION"):
                    relationships_list.append({ "relate_type": relationship.type,
                                                "floor": str(relationship["floor"]),
                                                "cost": relationship["cost"],
                                                "distance": relationship["distance"],
                                                "wait_time": relationship["wait_time"]                                                
                                            })

            # relationships_list = [{ "trans_type": relationship["type"], "relate_type": relationship.type} for relationship in relationships]

            # print("------NODES-------")
            # for node in nodes_list:
            #     print(node)
            # print("------RELATIONSHIPS-------")
            # for relationship in relationships_list:
            #     print(relationship)
        return nodes_list, relationships_list


if(__name__=="__main__"):
    uri = "neo4j+s://05c3b0e2.databases.neo4j.io"
    user = "neo4j"
    password = "K9JQB9KTNDYLRENAt8IoM2LEcSKQctkirZq2UJkGfwE"
    app = App(uri, user, password)
    # app.get_all_position_node()
    ### get nodes and relation of shortest path ###
    # nodes, relationships = app.get_shortest_path('room12_1',0,'room12_3',0)
    nodes, relationships = app.get_shortest_path('hotel_kitchen',0,'hotel_3rd_floor',3)
    for index, node in enumerate(nodes):
        print(node)
        # print("{0}_{1} ".format(node["at_map"], node["id_in_map"]))
        # print( "{0}_{1}| px: {2}, py: {3}, qz: {4}, qw: {5}".format( node["at_map"], node["id_in_map"], node["px"], node["py"], node["qz"], node["qw"]))
        if(index<len(nodes)-1):
            print("        |")
            print( "      {0}".format(relationships[index]) )
            # print( "      {0}-{1}".format(relationships[index]["trans_type"], relationships[index]["relate_type"]) )
            print("        |")
    app.close()