#!/usr/bin/env python3

import os
import glob
import rospkg
import yaml

# from node import node

from neo4j import GraphDatabase
import logging
from neo4j.exceptions import ServiceUnavailable


"""
Read all transition.yaml files from all map folders.
Generate into transition graph that link all maps together.
Create graph as graph databse with neo4j on server for now.


"""

class App:

    def __init__(self, uri, user, password):
        self.driver = GraphDatabase.driver(uri, auth=(user, password))

    def close(self):
        # Don't forget to close the driver connection when you are finished with it
        self.driver.close()

    ### POSITION NODE ### 
    def create_position_node(self, id_in_map, at_map, px, py, qz, qw, name=""):
        with self.driver.session() as session:
            # Write transactions allow the driver to handle retries and transient errors
            result = session.write_transaction(
                self._create_position_node, id_in_map, at_map, px, py, qz, qw, name)

    @staticmethod
    def _create_position_node(tx, id_in_map, at_map, px, py, qz, qw, name):
        query = (
            "CREATE (p:position { "
            "id_in_map: $id_in_map,"
            "at_map: $at_map,"
            "px: $px,"
            "py: $py,"
            "qz: $qz,"
            "qw: $qw,"
            "name: $name"
            "}) "
        )
        result = tx.run(query, id_in_map=id_in_map, at_map=at_map, px=px, py=py, qz=qz, qw=qw, name=name)
        return result
    
    ### GROUP NODE ###
    def create_group_node(self, group_id, at_map):
        with self.driver.session() as session:
            # Write transactions allow the driver to handle retries and transient errors
            result = session.write_transaction(
                self._create_group_node, group_id, at_map)

    @staticmethod
    def _create_group_node(tx, group_id, at_map):
        query = (
            "CREATE (n:group { "
            "group_id: $group_id,"
            "at_map: $at_map"
            "}) "
        )
        result = tx.run(query, group_id=group_id, at_map=at_map)
        return result

    ### TRANSITION NODE ###
    def create_transition_node(self, type_name, trans_id, in_map="", type_id="-1"):
        with self.driver.session() as session:
            # Write transactions allow the driver to handle retries and transient errors
            result = session.write_transaction(
                self._create_transition_node, type_name, trans_id, in_map, type_id)

    @staticmethod
    def _create_transition_node(tx, type_name, trans_id, in_map, type_id):
        query = (
            "CREATE (n:transition { "
            "trans_id: $trans_id,"
            "in_map: $in_map,"
            "type_name: $type_name,"
            "type_id: $type_id"
            "}) "
        )
        result = tx.run(query, type_name=type_name, type_id=type_id, trans_id=trans_id, in_map=in_map)
        return result

    ### IN_GROUP relationship ###
    def create_in_group_relationship(self, node_id, group_id, at_map):
        with self.driver.session() as session:
            result = session.write_transaction(self._create_in_group_relationship, node_id, group_id, at_map)

    @staticmethod
    def _create_in_group_relationship(tx, node_id, group_id, at_map):
        query = (
            "MATCH (n:position),(m:group) "
            "WHERE n.at_map=$at_map and m.at_map=$at_map and n.id_in_map=$node_id and m.group_id=$group_id "
            "CREATE (n)-[:IN_GROUP]->(m)"
        )
        result = tx.run(query, node_id=node_id, group_id=group_id, at_map=at_map)
        return result


    ### TRANSITION relationship ###
    """
    MATCH (u:position {id:$id_1, at_map:$map_1}), (r:position {id:$id_2, at_map:$map_2})
    CREATE (u)-[:TRANSITION {cost:$cost, distance:$distance, type:$type}]->(r)
    """

    def create_transition_relationship(self, position_id, position_map, trans_id, trans_map="", floor=0, cost=0, distance=0, wait_time=0):
        with self.driver.session() as session:
            result = session.write_transaction(self._create_transition_relationship, position_id, position_map, trans_id, trans_map, floor, cost, distance, wait_time)

    @staticmethod
    def _create_transition_relationship(tx, position_id, position_map, trans_id, trans_map, floor, cost, distance, wait_time):
        query = (
            "MATCH (u:position { id_in_map:$position_id, at_map:$position_map}), (r:transition {trans_id:$trans_id, in_map:$trans_map})"
            "CREATE (u)-[:TRANSITION { cost:$cost, distance:$distance, floor:$floor, wait_time:$wait_time}]->(r)"
        )
        # query = (
        #     "MATCH (u:position { id_in_map:$id_1, at_map:$map_1}), (r:position {id_in_map:$id_2, at_map:$map_2})"
        #     "CREATE (u)-[:TRANSITION { cost:$cost, distance:$distance, type:$type}]->(r)"
        # )
        result = tx.run(query, position_id=position_id, position_map=position_map, trans_id=trans_id, trans_map=trans_map, floor=floor, cost=cost, distance=distance, wait_time=wait_time)
    

class graph_database_generator:
    def __init__(self) -> None:
        rp = rospkg.RosPack()

        ### READ CONFIG FILE ### 
        config_file = "{}/config/config.yaml".format(rp.get_path('multi_map_planner'))
        try:
            with open(config_file) as f:
                self.config = yaml.safe_load(f)
        except Exception as e:
            print(e)

        ### GET MAP FOLDER PATH ###
        """
        MAPS folder will be in this format
        maps/
            transition_relationship.yaml    ***File that contain relationship of all transition points***
            <map name 1>/
                map.pbstream                Pose graph file for cartographer
                map.pgm                     Map's image
                map.yaml                    Map's metadata
                position.yaml             ***File that contain all points in map***
                position_relationship.yaml  ***File tjat contain all relationship in map***
            <map name 2>/
                                .
                                .
                                .
        """
        self.maps_path = "{0}/{1}".format(rp.get_path(self.config['map_package']), self.config['map_folder'])

        ## neo4j object ###
        self.uri = "bolt://localhost:11003"
        # self.uri = "bolt://localhost:7687"
        self.user = "neo4j"
        self.password = "12345678"
        # self.uri = "neo4j+s://05c3b0e2.databases.neo4j.io"
        # self.user = "neo4j"
        # self.password = "K9JQB9KTNDYLRENAt8IoM2LEcSKQctkirZq2UJkGfwE"
        self.app = App(self.uri, self.user, self.password)

    
    def init_map(self):
        map_path_list = glob.glob('{0}/*'.format(self.maps_path))
        map_name_list = [os.path.basename(x) for x in map_path_list]
        ### Get list of map name. Then iterate into each map folder for transition.yaml
        for map in map_name_list:
            if(map=="transition_relationship.yaml"):
                continue
            ### Read position node data from file ###
            position_path = "{0}/{1}/position.yaml".format(self.maps_path, map)
            with open(position_path, 'r') as file:
                positions = yaml.safe_load(file)
                file.close()
            ### Create position nodes ###
            for id in positions:
                px = positions[id]['pos']['px']
                py = positions[id]['pos']['py']
                qz = positions[id]['pos']['qz']
                qw = positions[id]['pos']['qw']
                name = ""
                if("name" in positions[id].keys()):
                    name = positions[id]['name']
                self.app.create_position_node(id_in_map=id,at_map=map,px=px,py=py,qz=qz,qw=qw,name=name)
            
            ### Read in-map relationship data from file
            position_relationship_path = "{0}/{1}/position_relationship.yaml".format(self.maps_path, map)
            with open(position_relationship_path, 'r') as file:
                data = yaml.safe_load(file)
                file.close()
            ### Create group/transition nodes and relationship ###
            for id in data:
                ### Create group ###
                if(data[id]["type"]=="group"):
                    group_id = data[id]["group_id"]
                    ### Node ###
                    self.app.create_group_node(group_id=group_id, at_map=map)
                    ### Relationship ###
                    for node_id in data[id]["locations"]:
                        self.app.create_in_group_relationship(node_id=node_id, group_id=group_id, at_map=map)
                ### Create in-map transition ###
                else:
                    trans_id = data[id]["trans_id"]
                    ### Transition parameters ###
                    floor = 0
                    if("floor" in data[id].keys()):
                        floor = data[id]['floor']
                    cost = 0
                    if("cost" in data[id].keys()):
                        cost = data[id]['cost']
                    distance = 0
                    if("distance" in data[id].keys()):
                        distance = data[id]['distance']
                    wait_time = 0
                    if("wait_time" in data[id].keys()):
                        wait_time = data[id]['wait_time']
                    self.app.create_transition_node(type_name=data[id]["type"], 
                            trans_id=trans_id, in_map=map)
                    for node_id in data[id]["locations"]:
                        self.app.create_transition_relationship(position_id=node_id, position_map=map, 
                                trans_id=trans_id, trans_map=map, floor=floor, cost=cost, distance=distance, wait_time=wait_time)


    def init_multi_map_relationship(self):
        data_path = "{0}/transition_relationship.yaml".format(self.maps_path)
        with open(data_path, 'r') as file:
            data = yaml.safe_load(file)
            file.close()
        for id in data:
            trans_id = data[id]["trans_id"]
            ### Create transition node ###
            self.app.create_transition_node(type_name=data[id]["type"], trans_id=trans_id)
            ### Transition parameters ###
            floor = 0
            if("floor" in data[id].keys()):
                floor = data[id]['floor']
            cost = 0
            if("cost" in data[id].keys()):
                cost = data[id]['cost']
            distance = 0
            if("distance" in data[id].keys()):
                distance = data[id]['distance']
            wait_time = 0
            if("wait_time" in data[id].keys()):
                wait_time = data[id]['wait_time']
            for location in data[id]["locations"]:
                position_map = location["map"]
                position_id = location["id"]
                if("floor" in location.keys()):
                    floor = location['floor']
                if("cost" in location.keys()):
                    cost = location['cost']
                if("distance" in location.keys()):
                    distance = location['distance']
                if("wait_time" in location.keys()):
                    wait_time = location['wait_time']
                self.app.create_transition_relationship(position_id=position_id, position_map=position_map, 
                        trans_id=trans_id, trans_map="", floor=floor, cost=cost, distance=distance, wait_time=wait_time)

    def init_graph(self):
        self.init_map()
        self.init_multi_map_relationship()
        self.app.close()

# if(__name__=="__main__"):
#     uri = "neo4j+s://05c3b0e2.databases.neo4j.io"
#     user = "neo4j"
#     password = "K9JQB9KTNDYLRENAt8IoM2LEcSKQctkirZq2UJkGfwE"
#     app = App(uri, user, password)
#     app.create_position_node(0,"map",0,0,0,1)
#     app.create_position_node(0,"map2",0,0,0,1)
#     app.create_transition_relationship(0, "map", 0, "map2", 0.5, 1, "door")
#     app.close()

if(__name__=="__main__"):
    generator = graph_database_generator()
    generator.init_graph()