from cmath import sqrt
import math
import numpy as np
import matplotlib.path as Path

"""
define circle radius and step size then use 
sliding windows approach to search through polygon 

** robot's front(X axis) facing elevator
** define robot's distance from elevator
"""

class Goal_search:
    def __init__(self, radius=0.4, step=0.1, lift_d=2.5, lift_w=2.5, lift_door_d=0.1) -> None:
        self.radius = radius
        self.step_size = step

        self.goals_center = []
        self.x = []
        self.y = []

        self.mean_goal = ()

        self.lift_d = lift_d ## Depth of elevator
        self.lift_w = lift_w ## Width of elevator
        self.lift_door_d = lift_door_d ## Depth of elevator's door

        self.skip = False

    def get_goal_inside(self, polygon, distance):
        """
        polygon: (x,y) data of all points, converted from lidar
        distance: distance from robot to elevator
        **assume robot's y is at center of elevator
        Find distance from each point to polygon's edges.
        """
        self.goals_center = []
        ### Start at most inner-left point ###
        x = self.lift_d + self.lift_door_d + distance - self.radius ## initial x value
        path = Path.Path(polygon)
        while(x > distance+self.radius+self.lift_door_d):
            y = (-1 * self.lift_w/2 )+ self.radius ## initial Y value
            while(y < (self.lift_w/2)-self.radius):
                self.skip = False
                if(path.contains_point((x,y))):
                    for i in range(-1,len(polygon)-1):
                        """
                        ## Check distance from x,y to polygon's edge (line segment)
                        ## For edge, use vertice at index i and i+1
                        x1,y1 = polygon[i]
                        x2,y2 = polygon[i+1]
                        """
                        d = self.minDistance(polygon[i], polygon[i+1], (x,y))
                        if(d.real < self.radius):
                            self.skip = True
                            break
                else:
                    self.skip = True
                        
                if(not self.skip):
                    self.goals_center.append((x,y))
                    self.x.append(x)
                    self.y.append(y)
                y+=self.step_size
            x-=self.step_size
            
        # return self.goals_center

    def set_robot_radius(self, radius):
        self.radius = radius

    def set_search_step(self, step):
        self.step = step

    def set_elevator_depth(self, depth):
        self.lift_d = depth

    def set_elevator_width(self, width):
        self.lift_w = width

    # Function to return the minimum distance
    # between a line segment AB and a point E
    ## ref: https://www.geeksforgeeks.org/minimum-distance-from-a-point-to-the-line-segment-using-vectors/
    def minDistance(self, A, B, E) :
    
        # vector AB
        AB = [None, None]
        AB[0] = B[0] - A[0]
        AB[1] = B[1] - A[1]
    
        # vector BP
        BE = [None, None]
        BE[0] = E[0] - B[0]
        BE[1] = E[1] - B[1]
    
        # vector AP
        AE = [None, None]
        AE[0] = E[0] - A[0]
        AE[1] = E[1] - A[1]
    
        # Variables to store dot product
    
        # Calculating the dot product
        AB_BE = AB[0] * BE[0] + AB[1] * BE[1]
        AB_AE = AB[0] * AE[0] + AB[1] * AE[1]
    
        # Minimum distance from
        # point E to the line segment
        reqAns = 0
    
        # Case 1
        if (AB_BE > 0) :
    
            # Finding the magnitude
            y = E[1] - B[1]
            x = E[0] - B[0]
            reqAns = sqrt(x * x + y * y)
    
        # Case 2
        elif (AB_AE < 0) :
            y = E[1] - A[1]
            x = E[0] - A[0]
            reqAns = sqrt(x * x + y * y)
    
        # Case 3
        else:
    
            # Finding the perpendicular distance
            x1 = AB[0]
            y1 = AB[1]
            x2 = AE[0]
            y2 = AE[1]
            mod = math.sqrt(x1 * x1 + y1 * y1)
            reqAns = abs(x1 * y2 - y1 * x2) / mod
        
        return reqAns

    def get_mean_point(self):
        sum_x = sum_y = 0
        for goal in self.goals_center:
            sum_x+=goal[0]
            sum_y+=goal[1]

        l = len(self.goals_center)
        return (sum_x/l, sum_y/l)