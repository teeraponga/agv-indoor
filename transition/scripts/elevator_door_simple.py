#!/usr/bin/env python3

import math
import rospy
import numpy as np
from sensor_msgs.msg import LaserScan

class test_elevator_door:
    def __init__(self,start=math.radians(45), end=math.radians(315)) -> None:
        if(end<0):
            end+=math.radians(360)
        self.required_angles = [(0,start), (end, math.radians(360))]

        ## Data for Laser scan after filter and rearrange to usable format
        self.frontal_scan = []  ## Cut and rearranged version of laser scan. Is in polar coordinate, use  with angle_increment and convert to cartesian.
        self.x = []     ## List of X value after convert to cartesian coordinate.
        self.y = []     ## List of Y value after convert to cartesian coordinate.
        self.polygon = None  ## List of polygon's vertices.

        ## Lidar-related variables
        self.laser_data = LaserScan()  ## Data from topic
        self.processing = False     ## Flag to stop receiving new data while calculating goal position
        self.laser_sub = rospy.Subscriber("/scan_filtered", LaserScan, self.scan_callback)
        self.laser_initialized = False  ## Flag to confirm that laserscan data is received
        self.angle_min = 0.0
        self.angle_max = 6.28318977355957
        self.angle_increment = 0.007863817736506462
        self.time_increment = 0.0
        self.scan_time = 0.0
        self.range_min = 0.15000000596046448
        self.range_max = 12.

        self.laser_sub = rospy.Subscriber("/scan_filtered", LaserScan, self.scan_callback)
        pass

    def scan_callback(self,data=LaserScan()):
        if(not self.processing):
            self.laser_data = data
        if(not self.laser_initialized):
            self.laser_initialized = True
            self.angle_min = data.angle_min
            self.angle_max = data.angle_max
            self.angle_increment = data.angle_increment
            self.time_increment = data.time_increment
            self.scan_time = data.scan_time
            self.range_min = data.range_min
            self.range_max = data.range_max
        self.filter_data(self.laser_data.ranges)
        self.check_door()

    def filter_data(self, raw):
        self.frontal_scan = []
        ### Cut only intereted section and then re-arrange it into new array
        for angle in self.required_angles:
            start_index = int(angle[0]/self.angle_increment)
            stop_index = int(angle[1]/self.angle_increment)
            tmp = list(raw[start_index:stop_index+1])
            tmp.reverse()
            for i in tmp:
                self.frontal_scan.append(i)

            ## frontal_only will be in angle range [ required_angles[0][1],  required_angles[1][0] - math.radians(360) ]
            # e.g. represent distance from angle [60 degree to -60 degree
            # Use polar to change from r and angle to x,y coordinate

            ### Convert from distance to x,y
            ## Treat robot's position as (0,0)
            self.x = []
            self.y = []
            tmp = []
            angle = self.required_angles[0][1]
            step = self.angle_increment
            for r in self.frontal_scan:
                if(r==math.inf):
                    continue
                x = r * math.cos(angle)
                y = r * math.sin(angle)
                self.x.append( x )
                self.y.append( y )
                if(abs(x)!=math.inf and abs(y)!=math.inf):
                    tmp.append([x,y])
                angle-=step
            self.polygon = np.asarray(tmp)

    def check_door(self):
        print(max(self.x))

if(__name__=="__main__"):
    from std_msgs.msg import Bool

    rospy.init_node("test_check_elevator_door")
    e = test_elevator_door()

    rospy.spin()
