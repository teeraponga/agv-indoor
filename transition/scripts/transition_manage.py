#! /usr/bin/env python3

import time
import roslib
roslib.load_manifest('transition')
import rospy
import actionlib

from transition.msg import TransitionAction, TransitionGoal, TransitionResult, TransitionFeedback

"""
Actionlib Server.
Use to manage robot transition 
"""

#TODO CHECK POSE AND POSESTAMPED MISS MATCH THAT GET SEND WHEN GETTING IN ELEVATOR AND FAIL

class Transition_Manage:
    def __init__(self) -> None:
        self._feedback = TransitionFeedback()
        self._result = TransitionResult()

        self.server = actionlib.SimpleActionServer('Transition', TransitionAction, self.execute, False)
        self.server.start()
        pass

    def execute(self, goal=TransitionGoal()):
        ### Elevator ###
        if(goal.type=="elevator"):
            print("Transition: elevator!!")
            from elevator import Elevator
            # ip =  "192.168.0.104" #"10.222.14.159"  #"192.168.114.2" #
            ip =  "localhost" 
            port = 5000
            
            e = Elevator(testing=False)
            e.setup(ip, port)
            time.sleep(0.5)
            e.set_current_floor(str(goal.current_floor))
            e.set_target_floor(str(goal.target_floor))
            res = e.elevator()
            while(res==404):
                ### Retry in case it failed to board the elevator 
                e.sio.disconnect()
                time.sleep(0.5)
                e.setup(ip, port)
                time.sleep(0.5)
                res = e.elevator()
            e.sio.disconnect()
            self._result.transition_result = "1" 
            self.server.set_succeeded(self._result)
        ### Door ###
        elif(goal.type=="door"):
            pass

if __name__ == '__main__':
    try:
        rospy.init_node('transition_server')
        server = Transition_Manage()
        rospy.spin()
    except KeyboardInterrupt:
        print("TERMINATED BY USER")

