#! /usr/bin/env python3


from get_in_out_elevator import Elevator_room_transition
import time

import socketio

from check_elevator_door import CheckElevatorDoor

"""
magictuch elevator
"""

class Elevator:
    def __init__(self, testing=False) -> None:
        self.sio = socketio.Client()

        self.testing_mode = testing
        if(not self.testing_mode):
            self.elevator_room_transition = Elevator_room_transition()

        self.state = 0
        """
        0: idle
        1: calling elevator
        2: getting in elevator
        3: going to target floor
        4: getting off elevator
        """

        self.elevator_success = False

        self.elevator_called = False
        self.elevator_commanded = False
        self.elevator_current_floor = ""

        self.robot_current_floor = ""
        self.robot_target_floor = ""

        self.elevator_door_checker = CheckElevatorDoor()

        self.is_in_elevator = False

        self.wait_elevator_count = 0
        self.wait_elevator_count_max = 3

    def connect(self, ip, port):
        self.sio.connect('http://{0}:{1}'.format(ip,port))
    
    def disconnect(self):
        self.sio.disconnect()

    def set_callback(self):
        @self.sio.on("robot_call")
        def on_robot_call(data):
            """ data : 
            "floor": String . Robot's current floor, for elevator to come and pick up.
            "robot_id": Int . Robot's unique id 
            """
            if(str(data["floor"])==str(self.robot_current_floor)):
                print("Confirm elevator will come to floor {}".format(data["floor"]))
                self.elevator_called = True
            else:
                print("Incorrect feedback from elevator, resend command...")
                self.call_elevator()
            
        @self.sio.on("robot_command")
        def on_robot_command(data):
            if(str(data["floor"])==str(self.robot_target_floor)):
                print("Elevator received target floor: {}".format(data["floor"]))
                self.elevator_commanded = True
            elif(data["floor"]=="H1"):
                print("Elevator received HOLD command.")
            elif(data["floor"]=="H0"):
                print("Elevator received UNHOLD command.")
            elif(data["floor"]=="C"):
                print("Elevator received CLOSE command.")
            else:
                print("Incorrect feedback from elevator, resend command")
                self.send_target_floor()


        @self.sio.on("robot_check")
        def on_robot_check(data):
            ## get current floor and response to robot
            # if(self.elevator_current_floor!=str(data["floor"])):
            #     print("Elevator is at floor: {}".format(data["floor"]))
            self.elevator_current_floor = str(data["floor"])

        @self.sio.on('my_message')
        def on_message(data):
            print('I received {}'.format(data))

    def setup(self, ip, port):
        self.set_callback()
        self.connect(ip,port)
    
    def set_current_floor(self, floor):
        self.robot_current_floor = str(floor)

    def set_target_floor(self, floor):
        self.robot_target_floor = str(floor)

    def call_elevator(self):
        self.elevator_called = False
        message = {"floor": str(self.robot_current_floor)}
        print("Calling elevator to floor {}...".format(self.robot_current_floor))
        self.sio.emit("robot_call", message)
    
    def hold_elevator(self):
        message = {"floor": "H1"}
        print("Set elevator to HOLD.")
        self.sio.emit("robot_command", message)

    def unhold_elevator(self):
        message = {"floor": "H0"}
        print("UNHOLD elevator before sending close command.")
        self.sio.emit("robot_command", message)

    def open_elevator(self):
        message = {"floor": "O"}
        print("Open elevator.")
        self.sio.emit("robot_command", message)

    def close_elevator(self):
        message = {"floor": "C"}
        print("Close elevator.")
        self.sio.emit("robot_command", message)

    def send_target_floor(self):
        self.elevator_commanded = False
        message = {"floor": str(self.robot_target_floor)}
        print("Set elevator target floor to floor {}.".format(self.robot_target_floor))
        self.sio.emit("robot_command", message)

    def check_elevator(self):
        self.sio.emit("robot_check")

    def elevator(self):
        """
        1. Robot call elevator. (Send command)
        2. Wait untill elevator arrive. (Loop check floor and elevator door status)
        3. Robot HOLD elevator. (Send command)
        4. Robot walk into elevator.
        5. Send target floor. (Send command)(Elevator will enter HOLD mode automatically)
        6. Cancel elevator HOLD mode. (Send command)
        7. Wait until reach target floor. (Loop check floor and elevator door status)
        8. Robot walk out off elevator.
        """
        ### Set starting condition ###
        self.elevator_success = False
        self.elevator_current_floor = -999
        print("Start!!")

        ### Call Elevator ###
        self.state = 1
        self.call_elevator()
        time.sleep(0.5)
        while(not self.elevator_called):
            ## wait for elevator to repeat call command
            ## TODO: Add timeout? any behavior when elevator not come?
            self.call_elevator()
            time.sleep(1)
            pass
        print("----")
        print("Elevator received calling command.")
        print("Waiting for elevator....")

        ### Check elevator floor and door status
        # self.elevator_door_checker.launch_laser_line_extraction()
        self.elevator_door_checker.register_subscriber()
        self.elevator_door_checker.set_initial_dist(1)
        while(str(self.elevator_current_floor)!=self.robot_current_floor and self.elevator_door_checker.status!=self.elevator_door_checker.OPEN):
            # print("Waiting")
            self.check_elevator()
            time.sleep(0.4)
        # self.elevator_door_checker.terminate_laser_line_extraction()
        self.elevator_door_checker.unregister_subscriber()

        """ OLD ELEVATOR FLOOR CHECKING METHOD
        self.wait_elevator_count = 0
        while(self.wait_elevator_count < self.wait_elevator_count_max):
            ## wait for elevator...  Use count to make sure elevator stop at robot's floor.
            if(str(self.elevator_current_floor)==self.robot_current_floor):
                self.wait_elevator_count+=1
            else:
                self.wait_elevator_count = 0
            print(self.wait_elevator_count)
            time.sleep(1)
            # self.check_elevator()
            pass
        """

        ### Open then Hold elevator before getting in ###
        print("----")
        print("Elevator arrived, set elevator to hold...")
        # self.open_elevator()
        # time.sleep(0.5)
        # self.open_elevator()
        # time.sleep(0.5)
        # self.open_elevator()
        # time.sleep(0.5)
        self.hold_elevator()
        time.sleep(0.5)
        self.hold_elevator()
        time.sleep(0.5)
        
        
        ### Getting in elevator
        self.state = 2
        print("Getting in elevator...")
        # CODE FOR GET IN s
        if(not self.testing_mode):
            result = 0
            for i in range(5):
                result = self.elevator_room_transition.get_in_elevator()
                if(self.elevator_room_transition.goal_found):
                    break
            if(result==404):
                print("Giving up getting in elevator. Retry in 10 seconds")
                # self.close_elevator()
                self.unhold_elevator()
                time.sleep(0.5)
                self.close_elevator()
                time.sleep(0.5)
                self.close_elevator()
                time.sleep(0.5)
                time.sleep(6)
                return 404
        else:
            # result = 0
            # result = self.elevator_room_transition.get_in_elevator(testing=True)
            # if(result==404):
            #     print("Giving up getting in elevator. Retry in 10 seconds")
            #     return 404
            time.sleep(5)

        ### After get in, send target floor then close elevator command and wait until arrive at target floor ###
        self.state = 3
        # self.unhold_elevator()
        time.sleep(0.5)
        self.send_target_floor()
        time.sleep(0.5)
        print("Set elevator target floor...")
        while(not self.elevator_commanded):
            ## wait for elevator to repeat command
            self.send_target_floor()
            time.sleep(1)
            pass
        time.sleep(0.5)
        # self.close_elevator()
        self.unhold_elevator()
        time.sleep(0.5)
        self.close_elevator()
        time.sleep(0.5)
        self.close_elevator()
        time.sleep(0.5)
        print("Going to target floor...")

        # self.elevator_door_checker.launch_laser_line_extraction()
        self.elevator_door_checker.register_subscriber()
        self.elevator_door_checker.set_initial_dist(0.5)
        while(str(self.elevator_current_floor)!=self.robot_target_floor and self.elevator_door_checker.status!=self.elevator_door_checker.OPEN):
            # print(self.wait_elevator_count)
            self.check_elevator()
            time.sleep(0.4)
        # self.elevator_door_checker.terminate_laser_line_extraction()
        self.elevator_door_checker.unregister_subscriber()

        """ OLD ELEVATOR FLOOR CHECKING METHOD
        self.wait_elevator_count = 0
        while(self.wait_elevator_count < self.wait_elevator_count_max):
            ## wait for elevator to arrive
            if(str(self.elevator_current_floor)==self.robot_target_floor):
                self.wait_elevator_count+=1
            else:
                self.wait_elevator_count = 0
            time.sleep(1)
            # self.check_elevator()
            pass
        """

        ### Arrived at target floor, get out of elevator ###
        # time.sleep(0.5)
        # self.open_elevator()
        # time.sleep(0.5)
        # self.open_elevator()
        # time.sleep(0.5)
        # self.open_elevator()
        # time.sleep(0.5)
        self.hold_elevator()
        time.sleep(0.5)
        self.hold_elevator()
        time.sleep(0.5)
        time.sleep(2)
        self.state = 4
        print("----")
        print("Arrived at target floor, getting off...")
        
        # CODE FOR GET OUT OFF ELEVATOR
        if(not self.testing_mode):
            self.elevator_room_transition.get_out_elevator()
        else:
            time.sleep(5)

        ### Send close command again release elevator
        # self.unhold_elevator()
        time.sleep(0.5)
        # self.close_elevator()
        self.unhold_elevator()
        time.sleep(0.5)
        self.close_elevator()
        time.sleep(0.5)
        self.close_elevator()
        time.sleep(0.5)
        
        self.state = 0
        self.elevator_success = True
        print("Target floor reached.")
        print("TERMINATING!!!")
        return 0


if(__name__=="__main__"):

    ip = "192.168.0.103" #"localhost"
    port = 5000
    
    e = Elevator(testing=True)
    e.setup(ip, port)
    time.sleep(0.5)
    e.set_current_floor("1")
    e.set_target_floor("3")

    mode = 1
    t = time.time()
    try:
        if(mode==0):
            ## ALL WORKFLOW
            e.elevator()
        elif(mode==1):
            ## CHECK ONLY
            
            @e.sio.on("robot_check")
            def on_robot_check(data):
                global t
                ## get current floor and response to robot
                print("{0}, floor {1}".format(time.time()-t,data["floor"]))
                t = time.time()
                # if(e.elevator_current_floor!=str(data["floor"])):
                #     print("Elevator is at floor: {}".format(data["floor"]))
                e.elevator_current_floor = str(data["floor"])
            input('Press ENTER to exit...')
        elif(mode==2):
            ## KEEP CALLING
            while(True):
                e.call_elevator()
                time.sleep(1)
        
        elif(mode==3):
            ## HOLD, UNHOLD, CLOSE
            e.hold_elevator()
            time.sleep(3)
            e.unhold_elevator()
            time.sleep(0.5)
            e.close_elevator()
            time.sleep(0.5)

    except KeyboardInterrupt:
        print("Terminating")

    
    e.sio.disconnect()

