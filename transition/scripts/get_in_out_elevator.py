#!/usr/bin/env python3

import rospy
import math
import numpy as np
import time

from get_elevator_goal import Goal_search
from sensor_msgs.msg import LaserScan
from actionlib_msgs.msg import GoalStatusArray
from geometry_msgs.msg import PoseStamped

from std_srvs.srv import Empty

from point_navigation import Point_navigation

"""
monitor goal status (sent, reached, error)
publish goal
fallback beavior
"""

class Elevator_room_transition:
    def __init__(self,start=math.radians(45), end=math.radians(315), robot_radius=0.33, inflation=0.1) -> None:
        ## Angle range to use.
        if(end<0):
            end+=math.radians(360)
        self.required_angles = [(0,start), (end, math.radians(360))]

        ## Robot's radian with inflation. Use to calculate valid goal position inside elevator
        self.circle_radius = robot_radius + inflation

        ## Data for Laser scan after filter and rearrange to usable format
        self.frontal_scan = []  ## Cut and rearranged version of laser scan. Is in polar coordinate, use  with angle_increment and convert to cartesian.
        self.x = []     ## List of X value after convert to cartesian coordinate.
        self.y = []     ## List of Y value after convert to cartesian coordinate.
        self.polygon = None  ## List of polygon's vertices.

        ## Lidar-related variables
        self.laser_data = LaserScan()  ## Data from topic
        self.processing = False     ## Flag to stop receiving new data while calculating goal position
        self.laser_sub = rospy.Subscriber("/scan_filtered", LaserScan, self.scan_callback)
        self.laser_initialized = False  ## Flag to confirm that laserscan data is received
        self.angle_min = 0.0
        self.angle_max = 6.28318977355957
        self.angle_increment = 0.007863817736506462
        self.time_increment = 0.0
        self.scan_time = 0.0
        self.range_min = 0.15000000596046448
        self.range_max = 12.

        ## Navigation-related variables
        self.navigation = Point_navigation() ## Class to help manage move base navigation
        self.goal_msg = PoseStamped()   ## Goal postion message to publish
        self.goal_msg.header.frame_id = "base_footprint"    ## Set to robot's frame because elevator goal is calculated with respect to robot

        ## Variables for goal in elevator
        self.searcher = Goal_search(lift_d=1.6, lift_w=1.6, lift_door_d=0.6)
        self.goal_tuple = (0,0)
        self.goal_found = False

        self.finish = False

    
    def scan_callback(self,data=LaserScan()):
        if(not self.processing):
            self.laser_data = data
        if(not self.laser_initialized):
            self.laser_initialized = True
            self.angle_min = data.angle_min
            self.angle_max = data.angle_max
            self.angle_increment = data.angle_increment
            self.time_increment = data.time_increment
            self.scan_time = data.scan_time
            self.range_min = data.range_min
            self.range_max = data.range_max

    def filter_data(self, raw):
        self.frontal_scan = []
        ### Cut only intereted section and then re-arrange it into new array
        for angle in self.required_angles:
            start_index = int(angle[0]/self.angle_increment)
            stop_index = int(angle[1]/self.angle_increment)
            tmp = list(raw[start_index:stop_index+1])
            tmp.reverse()
            for i in tmp:
                self.frontal_scan.append(i)

            ## frontal_only will be in angle range [ required_angles[0][1],  required_angles[1][0] - math.radians(360) ]
            # e.g. represent distance from angle [60 degree to -60 degree
            # Use polar to change from r and angle to x,y coordinate

            ### Convert from distance to x,y
            ## Treat robot's position as (0,0)
            self.x = []
            self.y = []
            tmp = []
            angle = self.required_angles[0][1]
            step = self.angle_increment
            for r in self.frontal_scan:
                x = r * math.cos(angle)
                y = r * math.sin(angle)
                self.x.append( x )
                self.y.append( y )
                if(abs(x)!=math.inf and abs(y)!=math.inf):
                    tmp.append([x,y])
                angle-=step
            self.polygon = np.asarray(tmp)

    def get_in_elevator(self, testing=False):
        self.finish = False
        ## 1. Get laser scan data.
        if(self.laser_initialized):
            self.processing = True ## To prevent change to laser data while calculating
            ## 2. Filter to correct format (array of x and y. or to say a polygon)
            self.filter_data(self.laser_data.ranges)
            ## 3. Calculate goal position inside elevator. Goal position will be x,y relative to robot.
            self.goal_found = False
            for i in range(3): ## Try looks for goal n times. wait n seconds if no goal found.
                self.searcher.get_goal_inside(self.polygon, distance=min(self.x))
                if(len(self.searcher.goals_center)>0):
                    self.goal_tuple = self.searcher.get_mean_point()
                    self.goal_found = True
                    break
                else:
                    ## No valid goal found, wait n second then retry
                    time.sleep(0.8)
                    pass
            self.processing = False
            if(not self.goal_found):
                print("Elevator full, cannot get in...")
                return 404
        else:
            print("LIDAR not initialized..")
            return
        if(testing):
            return
        ## 4. Set goal message. And remember robot's current position to use when get out of elevator
        self.goal_msg.pose.position.x = self.goal_tuple[0]
        self.goal_msg.pose.position.y = self.goal_tuple[1]
        self.goal_msg.pose.orientation.w = 1.0
        ## 5. Send goal to move base. 
        self.navigation.set_fallback_mode(self.navigation.GO_TO_PREVIOUS_POSE)
        self.navigation.send_goal(self.goal_msg)
        ## 6. Wait until move base finished.
        while(not self.navigation.goal_reached):
            # if(self.navigation.goal_error):
            #     ## Perform any fallback behavior
            #     print("Cannot get in elevator. ")
            #     break
            # elif(self.navigation.goal_reached):
            #     print("Robot is inside elevator")
            #     break
            time.sleep(0.3)
        if(self.navigation.fallback_trigger):
            self.navigation.reset_fallback_trigger()
            return 404
        ## 7. Rotate 180 degree.
        self.goal_msg.pose.position.x = 0.0
        self.goal_msg.pose.position.y = 0.0
        self.goal_msg.pose.orientation.z = 1.0
        self.goal_msg.pose.orientation.w = 0.0
        self.navigation.send_goal(self.goal_msg)
        while(not self.navigation.goal_reached):
            time.sleep(0.3)
            pass
        ## FINISH GETTING IN
        self.finish = True
        return 1

    def get_out_elevator(self, testing=False):
        self.finish = False
        ## 1. Set robot's goal to outside elevator (calculate from pose before get in elevator)
        self.goal_msg.pose.position.x = self.goal_tuple[0]
        self.goal_msg.pose.position.y = self.goal_tuple[1]
        self.goal_msg.pose.orientation.z = 0.0
        self.goal_msg.pose.orientation.w = 1.0
        ## 2. Send that position as goal to move base
        self.navigation.clear_costmap_service_proxy.call()  ## Clear costmap fisrt to remove lift door in case lidar can't clear by itself
        time.sleep(1.0)
        self.navigation.set_fallback_mode(self.navigation.RESEND_GOAL)
        self.navigation.send_goal(self.goal_msg)
        ## 3. Wait until move base finished.
        while(not self.navigation.goal_reached):
            # if(self.navigation.goal_error):
            #     ## Perform any fallback behavior
            #     print("Cannot get out off elevator. ")
            #     break
            # elif(self.navigation.goal_reached):
            #     print("Robot is now outside elevator.")
            #     break
            time.sleep(0.3)
        ## FINISH GETTING OUT
        self.finish = True


##### Create a subscriber wait for topic to get in/out elevator #####
if(__name__=="__main__"):
    from std_msgs.msg import Bool

    rospy.init_node("elevator_get_in_out")
    e = Elevator_room_transition()

    ### True: get in elevator
    ### Flse: get out elevator
    def callback(data=Bool()):
        if(data.data):
            e.get_in_elevator()
        else:
            e.get_out_elevator()

    sub = rospy.Subscriber("get_in_or_out", Bool, callback=callback)
    rospy.spin()
