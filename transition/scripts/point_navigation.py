#!/usr/bin/env python3

import time
import math
import rospy
from actionlib_msgs.msg import GoalStatusArray
from geometry_msgs.msg import Pose, PoseStamped

from std_srvs.srv import Empty

"""
monitor goal status (sent, reached, error)
publish goal
fallback beavior
"""

class Point_navigation:
    def __init__(self) -> None:
        self.goal_topic = "/move_base_simple/goal"
        self.goal_status_topic = "move_base/status"
        self.goal_pub = rospy.Publisher(self.goal_topic, PoseStamped, queue_size=10)
        self.goal_status_sub = None # rospy.Subscriber("move_base/status", GoalStatusArray, self.goal_status_callback)
        self.current_goal = PoseStamped()
        self.goal_reached = False
        self.goal_sent = False
        self.goal_error = False

        self.robot_pose_topic = "/robot_pose"
        self.robot_pose = PoseStamped()
        self.robot_pose.header.frame_id = 'base_footprint'
        self.robot_pose_sub = None
        self.robot_pose_received = False

        ### FALLBACK MODES ###
        self.ABORT = 0  ## Simply end current goal.
        self.RESEND_GOAL = 1  ## Resend same goal and try to navigate again. for important node.
        self.TOLERATE_DISTANCE = 2  ## If robot's position is not too far from goal, treat as navigation finished. If not, simply resend goal.
        self.tolerable_distance = 1.0 ## tolerate distance (m) 
        self.GO_TO_FALLBACK_POSE = 3  ## Go to fallback position instead.
        self.GO_TO_PREVIOUS_POSE = 4  ## Go to position before navigation
        ### ### ###

        self.fallback_trigger = False
        self.fallback_mode = self.RESEND_GOAL
        self.fallback_pose = PoseStamped()

        ### Clear Costmap
        rospy.wait_for_service('/move_base/clear_costmaps')
        self.status_sub = rospy.Subscriber("move_base/status",GoalStatusArray, self.status_callback)
        self.clear_costmap_service_proxy = rospy.ServiceProxy('/move_base/clear_costmaps', Empty)

    def status_callback(self,data):
        pass

    def goal_status_callback(self, data):
        if(len(data.status_list)==0):
            return
        elif(data.status_list[-1].status==1 and not self.goal_sent):
            ### confirmed movebase received goal
            self.goal_sent = True
            self.goal_error = False
            print("goal sent")
        elif(data.status_list[-1].status==3 and self.goal_sent and not self.goal_reached):
            ### goal reached
            self.goal_reached = True
            self.goal_error = False
            self.fallback_trigger = False
            print("goal reached")
            self.goal_status_sub.unregister()
        elif(data.status_list[-1].status==4 and self.goal_sent and not self.goal_error):
            ### goal error
            self.goal_error = True
            print("goal stuck")
            self.fallback_function()
    
    def send_goal(self, pose=Pose(), frame='map'):
        goal_to_send = PoseStamped()
        goal_to_send.header.stamp = rospy.Time.now()
        goal_to_send.header.frame_id = frame
        goal_to_send.pose = pose
        self.send_goal(goal_to_send)

    def send_goal(self, goal=PoseStamped(), fallback=False):
        if(not fallback):
            self.fallback_pose = goal
        self.current_goal = goal
        self.goal_pub.publish(self.current_goal)
        ### wait for confirm from movebase before set to true
        self.goal_sent = False
        self.goal_reached = False
        self.goal_status_sub = rospy.Subscriber(self.goal_status_topic, GoalStatusArray, self.goal_status_callback)

    ##################
    ### ROBOT POSE ###
    def robot_pose_callback(self, data=Pose()):
        self.robot_pose.pose = data
        self.robot_pose_received = True

    def register_robot_pose_subscriber(self):
        self.robot_pose_sub = rospy.Subscriber(self.robot_pose_topic, Pose, self.robot_pose_callback)

    def unregister_robot_pose_subscriber(self):
        self.robot_pose_sub.unregister()
    ##################

    #########################
    ### FALLBACK BEHAVIOR ###

    def reset_fallback_trigger(self):
        self.fallback_trigger = False

    def set_fallback_pose(self, pose=Pose(), frame=''):
        self.fallback_pose.pose = pose
        if(frame!=''):
            self.fallback_pose.header.frame_id = frame

    def set_tolerable_distance(self, tolerable_distance=1.0):
        self.tolerable_distance = tolerable_distance

    def set_fallback_mode(self, mode, pose=Pose(), frame='map', tolerable_distance=1.0):
        self.fallback_mode = mode
        if(mode==self.GO_TO_FALLBACK_POSE):
            self.set_fallback_pose(pose=pose, frame=frame)
        elif(mode==self.TOLERATE_DISTANCE):
            self.set_tolerable_distance(tolerable_distance)
        elif(mode==self.GO_TO_PREVIOUS_POSE):
            self.register_robot_pose_subscriber()
            while(not self.robot_pose_received):
                time.sleep(0.5)
            self.unregister_robot_pose_subscriber()
            self.robot_pose_received = False

    def fallback_function(self):
        ### function to run in case goal error
        self.fallback_trigger = True
        ### ABORT ###
        if(self.fallback_mode==self.ABORT):
            self.goal_status_sub.unregister()
        ### RESEND SAME GOAL ###
        elif(self.fallback_mode==self.RESEND_GOAL):
            self.send_goal(self.current_goal)
        ### CHECK DISTANCE TO GOAL ###
        elif(self.fallback_mode==self.TOLERATE_DISTANCE):
            self.register_robot_pose_subscriber()
            while(not self.robot_pose_received):
                time.sleep(0.5)
            self.unregister_robot_pose_subscriber()
            x1 = self.robot_pose.pose.position.x
            y1 = self.robot_pose.pose.position.y
            x2 = self.current_goal.pose.position.x
            y2 = self.current_goal.pose.position.y
            if(math.dist((x1,y1),(x2,y2))<=self.tolerable_distance):
                self.goal_reached = True
                self.goal_error = False
            else:
                self.send_goal(self.current_goal)
        ### GO TO FALLBACK POSITION ###
        elif(self.fallback_mode==self.GO_TO_FALLBACK_POSE):
            self.send_goal(self.fallback_pose)
        elif(self.fallback_mode==self.GO_TO_PREVIOUS_POSE):
            self.robot_pose.header.stamp = rospy.Time.now()
            self.send_goal(self.robot_pose)

    #########################
