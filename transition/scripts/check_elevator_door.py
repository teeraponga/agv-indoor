#!/usr/bin/env python3

import math
import roslaunch
import rospkg
import rospy
from laser_line_extraction.msg import  LineSegmentList

class CheckElevatorDoor:
    def __init__(self,start=math.radians(45), end=math.radians(315), big_thresh=0.5, small_thresh=0.1, max_count=3, initial_dist=1, angle_offset=math.pi) -> None:
        self.line_sub = None #rospy.Subscriber("/line_segments", LineSegmentList, self.line_callback)

        pkg = rospkg.RosPack()
        self.laser_line_extraction_pkg_path = pkg.get_path("laser_line_extraction")
        self.laser_line_extraction_launch_file = "example.launch"
        self.laser_line_extraction_launch = None

        self.start = start
        self.end = end

        self.angle_offset = angle_offset

        self.initial_dist = initial_dist
        self.initialized = False

        self.CLOSE = 0
        self.OPEN = 1
        self.status = self.CLOSE

        self.max_line_dist = 0
        self.big_thresh = big_thresh
        self.small_thresh = small_thresh
        self.big_count = 0
        self.small_count = 0
        self.max_count = max_count

    def launch_laser_line_extraction(self):
        self.status = self.CLOSE
        uuid = roslaunch.rlutil.get_or_generate_uuid(None, False)
        self.laser_line_extraction_launch = roslaunch.parent.ROSLaunchParent(uuid, ["{0}/launch/{1}".format(self.laser_line_extraction_pkg_path, self.laser_line_extraction_launch_file)])
        self.laser_line_extraction_launch.start()

    def terminate_laser_line_extraction(self):
        self.initialized = False
        self.laser_line_extraction_launch.shutdown()

    def line_callback(self,data=LineSegmentList()):
        # print("---")
        max_dist = 0
        for line in data.line_segments:
            """ line_segment message 
            float32 radius
            float32 angle
            float32[4] covariance
            float32[2] start
            float32[2] end
            """
            if(line.angle+self.angle_offset < self.start or line.angle+self.angle_offset > self.end and line.radius > max_dist):
                max_dist = line.radius
            #     print("    rad:{}".format(line.radius))
            #     print("    ang:{}".format(line.angle))
            #     print("    ---")

        if(not self.initialized):
            self.max_line_dist = max_dist
            self.initialized = True
            if(self.max_line_dist-self.initial_dist > self.big_thresh ):
                self.status = self.OPEN
                print("Start as open")

        if(abs(max_dist-self.max_line_dist)>self.big_thresh):
            self.big_count+=1
            self.small_count = 0
            if(self.big_count>=self.max_count):
                if(max_dist>self.max_line_dist):
                    ### max dist increase = Elevator open
                    self.status = self.OPEN
                    # print("open")
                elif(max_dist<self.max_line_dist):
                    ### max dist decrease = Elevator close
                    self.status = self.CLOSE
                    # print("close")
                self.max_line_dist = max_dist
                self.big_count = 0
        elif(abs(max_dist-self.max_line_dist)>self.small_thresh):
            self.small_count+=1
            self.big_count = 0
            if(self.small_count>=self.max_count):
                self.max_line_dist = max_dist
                self.small_count = 0
        else:
            self.big_count = 0
            self.small_count = 0
        # print(max_dist)

    def set_initial_dist(self, dist):
        self.initial_dist = dist
    
    def set_state(self, state):
        self.status = state

    def set_angle_offset(self, angle_offset):
        self.angle_offset = angle_offset

    def register_subscriber(self):
        self.status = self.CLOSE
        self.line_sub = rospy.Subscriber("/line_segments", LineSegmentList, self.line_callback)
    
    def unregister_subscriber(self):
        self.initialized = False
        self.line_sub.unregister()


if(__name__=="__main__"):
    rospy.init_node("test_elevator_door")
    
    ted = CheckElevatorDoor(angle_offset=0)
    state = ["close", "open"]
    previous_state = ted.status
    rate = rospy.Rate(10)
    ted.launch_laser_line_extraction()
    ted.register_subscriber()
    print("started")
    while(not rospy.is_shutdown()):
        if(ted.status!=previous_state):
            print(state[ted.status])
            previous_state = ted.status
        rate.sleep()

    ted.terminate_laser_line_extraction()
    