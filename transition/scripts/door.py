#! /usr/bin/env python3

from geometry_msgs.msg import Twist
import time
import rospy

class Door:
    def __init__(self) -> None:
        self.vel_pub = rospy.Publisher("/trans_vel", Twist, queue_size=10)
        self.count = 0
        self.hz = 10
        self.rate = rospy.Rate(self.hz)

        self.vel = Twist()
        self.vel.linear.x = 0.3

    def door(self, wait_time, distance):
        self.count = 0
        max_count = int((distance/self.vel.linear.x) * self.hz)
        print(max_count)
        time.sleep(wait_time)
        while(not rospy.is_shutdown() and self.count < max_count):
            self.vel_pub.publish(self.vel)
            time.sleep(1/self.hz)
            self.count+=1


if(__name__=="__main__"):
    rospy.init_node("door")
    d = Door()
    d.door(3,5)
    print("done")