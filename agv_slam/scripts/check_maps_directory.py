#!/usr/bin/env python3

import os
import rospy

"""
Receive map package path and map name
Check whether directory with map name exist
If not exit, create a directory
"""

path = rospy.get_param("check_path")

if(not os.path.isdir(path)):
    os.mkdir(path)
    print("Directory created.")
else:
    print("Directory already existed.")