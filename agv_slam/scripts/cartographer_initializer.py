#!/usr/bin/env python3
import rospy
import roslaunch
import subprocess
import signal
from geometry_msgs.msg import PoseWithCovarianceStamped
from tf.transformations import euler_from_quaternion
import tf2_ros

"""
Subscribe PoseWithCovarianceStamped message
Call service finish trajectory with lst id to remove cartographer latest trajectory
Call service start trajectory with pose data from subsriber and lua config path
"""

last_id = 1
use_initial_pose = False
x, y, qz, qw = 0, 0, 0, 0
configuration_directory = ""
configuration_basename = ""


def init_callback(data):
    global last_id, use_initial_pose, x, y, qz, qw
    use_initial_pose = True
    global configuration_directory
    global configuration_basename

    x = data.pose.pose.position.x
    y = data.pose.pose.position.y
    qz = data.pose.pose.orientation.z
    qw = data.pose.pose.orientation.w

    finish_traj = subprocess.Popen(["rosservice", "call", "/finish_trajectory", str(last_id)])
    rospy.sleep(0.5)
    start_new_traj = subprocess.Popen(["rosservice", "call", "/start_trajectory",
                                    #    "configuration_directory: '{}'".format(configuration_directory),
                                    #    "configuration_basename: '{}'".format(configuration_basename),
                                    #    "use_initial_pose: {}".format(use_initial_pose),
                                       
                                    #    """initial_pose: 
                                    #         position: {0}x: {1}, y: {2}, z: 0.0{3}
                                    #         orientation: {0}x: 0.0, y: 0.0, z: {4}, w: {5}{3}""".format('{', x, y, '}', qz, qw),
                                    #     "relative_to_trajectory_id: 0"])
                                    """
                                    configuration_directory: '{2}'
                                    configuration_basename: '{3}'
                                    use_initial_pose: true
                                    initial_pose:
                                        position: {0}x: {4}, y: {5}, z: 0.0{1}
                                        orientation: {0}x: 0.0, y: 0.0, z: {6}, w: {7}{1}
                                    relative_to_trajectory_id: 0
                                    """.format('{', '}', configuration_directory, configuration_basename, x, y, qz, qw)
    ])
    # start_new_traj = subprocess.Popen(["rosservice", "call", "/start_trajectory", 
    #     "\"configuration_directory: '{0}' configuration_basename: '{1}' use_initial_pose: {2} initial_pose: position: {3}x: {4}, y: {5}, z: 0.0{6} orientation: {7}x: 0.0, y: 0.0, z: {8}, w: {9}{10} relative_to_trajectory_id: 0\"".format(configuration_directory,configuration_basename,use_initial_pose,'{', x, y, '}','{', qz, qw, '}'),])

    last_id += 1


rospy.init_node("cartographer_initializer")

configuration_directory = rospy.get_param("~configuration_directory", default="")
configuration_basename = rospy.get_param("~configuration_basename", default="")

rospy.Subscriber("/initialpose", PoseWithCovarianceStamped, init_callback)

rospy.spin()
