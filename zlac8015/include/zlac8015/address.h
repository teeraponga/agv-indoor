#ifndef ADDRESS_H
#define ADDRESS_H

struct commands 
{
    float       firmware_version;   // AGV_Education Software Version
    // ODOM
    float       x;                  // unit : meter
    float       y;                  // unit : meter
    float       theta;              // unit : rad
    float       linear_vel_x;       // unit : m/s
    float       linear_vel_y;       // unit : m/s
    float       angular_vel_theta;  // unit : rad/s
    // COMMAND
    float       Vx_robot;           // unit : m/s
    float       Vy_robot;           // unit : m/s
    float       Omega_robot;        // unit : rad/s
};  // sizeof(REGISTER_TABLE) = hex28 = dec40 -----> 0x00 - 0x27

enum ADDRESS
{
    /// address to read from
    MOTOR_TEMPERATURE       = 0x2026, // *0.1 celcius
    MOTOR_STATUS            = 0x2027, // 0:stationary,  1:running
    HALL_INPUT              = 0x2028, // 0 and 7 mean hall sensor error
    VOLTAGE                 = 0x2029, // *0.01 V
    FEEDBACK_POSITION_HIGH  = 0x202A, // motor position high byte, unit: count
    FEEDBACK_POSITION_LOW   = 0x202B, // motor position low byte, unit: count
    FEEDBACK_SPEED          = 0x202C, // *0.1 RPM
    FEEDBACK_TORQUE         = 0x202D, // *0.1A range [-300, 300]
    ERROR_CODE              = 0x202E, // See {enum PROTOCOL_ERROR}
    CONNECTION              = 0x202F, // 1: connected, 0: disconnected
    /// address to write to
    CONTROL                 = 0x2031, // See {enum CONTROL_WORD}
    MODE                    = 0x2032, // See {enum OPERATING_MODE}
    TARGET_TORQUE           = 0x2033, // unit: mA, range [-30000, 30000]
    TARGET_POSITION_HIGH    = 0x2034, // motor position high byte, unit: count
    TARGET_POSITION_LOW     = 0x2035, // motor position low byte, combined range [-1000000, 1000000]
    MAX_SPEED               = 0x2036, // max speed only for position mode
    ACC                     = 0x2037, // acceleration time, range [0,32767], unit ms
    DCC                     = 0x2038, // deceleration time, **less = faster acc/dcc time 
    EMER_STOP_DCC           = 0x2039, //
    TARGET_SPEED            = 0x203A, // target velocity in velocity mode
    /// more later ///
};
enum FUNCTION
{
    READ            = 0x03,
    WRITE           = 0x06,
    WRITE_MULTIPLE  = 0x10,
};
enum PROTOCOL_ERROR
{
    NO_ERROR                = 0x0000,
    OVER_VOLTAGE            = 0x0001,
    UNDER_VOLTAGE           = 0x0002,
    OVER_CURRENT            = 0x0004,
    OVERLOAD                = 0x0008,
    CURRENT_OUT_TOLERANCE   = 0x0010,
    ENCODER_OUT_TOLERANCE   = 0x0020,
    SPEED_OUT_TOLERANCE     = 0x0020,
    REF_VOLTAGE_ERROR       = 0x0020,
    EEPROM_ERROR            = 0x0020,
    HALL_ERROR              = 0x0020,
    HIGH_TEMPERATURE        = 0x0020,
};
enum CONTROL_WORD
{
    ALARM_CLEAR             = 0x06,
    STOP                    = 0x07,
    ENABLE                  = 0x08,
    START                   = 0x10, // only for position mode
};
enum OPERATING_MODE
{
    UNDEFINED               = 0,
    ABSOLUTE_POSITION       = 1,
    RELATIVE_POSITION       = 2,
    VELOCITY                = 3,
    TORQUE                  = 4,
};
#endif
