#ifndef ZLAC8015_H
#define ZLAC8015_H

struct commands 
{
    float       firmware_version;   // AGV_Education Software Version
    // ODOM
    float       x;                  // unit : meter
    float       y;                  // unit : meter
    float       theta;              // unit : rad
    float       linear_vel_x;       // unit : m/s
    float       linear_vel_y;       // unit : m/s
    float       angular_vel_theta;  // unit : rad/s
    // COMMAND
    float       Vx_robot;           // unit : m/s
    float       Vy_robot;           // unit : m/s
    float       Omega_robot;        // unit : rad/s
};

namespace _ZLAC8015 
{
    /*
        this class handle the creatioin of mes
    */
    class zlac8015
    {
        private:
            /* data */
            int _address;           // address in RS485
            int _mode;              // operating mode
            float _feedback_speed;  // current motor speed

            //////////////////// remove these 3 until figured out serial method(COM port, tx rx, etc)
            /*
                send msg(byte array) through serial(rs485)
            */
            // bool _write(int,int,int);  // (rs485 address, registor address, data)
            
            /*
                read msg(byte array) from serial(rs485)
            */
            // bool _read(int, int, int); // (rs485 address, starting address, byte(s) to read)
            
            /*
                send msg(byte array) through serial(rs485)
            */
            // bool _writeMultiple(int, int, int, int, int); //(rs485 address, staring address, ?, bytes, data)

            /*
                calculate crc16-MODBUS for each byte array
            */
            int _crcCalc();

        public:
            zlac8015();
            ~zlac8015();

            bool setMode(int, int);    // (rs485 address, mode)
            bool setSpeed(int, int);   // (rs485 address, speed(rpm))
            bool setAcc(int, int);     // (rs485 address, acc(ms))
            bool setDcc(int, int);     // (rs485 address, dcc(ms))
            bool setEnable(int, bool); // (rs485 address, T/F)

            bool clearAlarm(int); // (rs485 address)
    };
    
    
}

#endif