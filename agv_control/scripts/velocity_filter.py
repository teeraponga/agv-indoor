#!/usr/bin/env python3

import rospy
from geometry_msgs.msg import Twist, TwistStamped
from std_msgs.msg import Bool

"""
Trapesodial velocity filter
"""

class velocity_filter:
    def __init__(self) -> None:
        self.time_step = 0.1
        self.previous_time = rospy.Time.now().to_sec()
        
        self.limit_lin_safety_forward = 0.5
        self.limit_lin_safety_backward = 0.5
        self.limit_lin_vel = 1.0
        self.limit_lin_acc = 1.0
        self.limit_ang_vel = 1.0
        self.limit_ang_acc = 1.0
        self.update_params()

        self.current_vel = Twist()
        self.target_vel = Twist()
        self.vel_msg = Twist()
        self.vel_pub = rospy.Publisher("cmd_vel", Twist, queue_size=10)
        self.target_vel_sub = rospy.Subscriber("mux_vel", Twist, self.target_vel_callback)
        self.feedback_vel_sub = rospy.Subscriber("feedback_vel", TwistStamped, self.feedback_vel_callback)
        self.emergency_brake_sub = rospy.Subscriber("joy_pause", Bool, self.brake_callback)
        self.brake = False

    def brake_callback(self, data):
        self.brake = data.data
        if(self.brake):
            self.reset_vel()
            self.vel_pub.publish(self.vel_msg)

    def reset_vel(self):
        self.current_vel = Twist()
        self.target_vel = Twist()
        self.vel_msg = Twist()


    def feedback_vel_callback(self, data):
        # self.time_step = data.header.stamp.to_sec() - self.previous_time
        # self.previous_time = data.header.stamp.to_sec()
        self.current_vel = data.twist

    def target_vel_callback(self, data):
        ### filter velocity to not exceed limit ###
        self.target_vel = data
        if(self.target_vel.linear.x >= 0):
            self.target_vel.linear.x = min(self.target_vel.linear.x, min( self.limit_lin_vel, self.limit_lin_safety_forward))
        elif(self.target_vel.linear.x < 0):
            self.target_vel.linear.x = max(self.target_vel.linear.x, -1 * min( self.limit_lin_vel, self.limit_lin_safety_backward))

        if(self.target_vel.angular.z >= 0):
            self.target_vel.angular.z = min(self.target_vel.angular.z, self.limit_ang_vel)
        elif(self.target_vel.angular.z < 0):
            self.target_vel.angular.z = max(self.target_vel.angular.z, -1*self.limit_ang_vel)

        

    def update_vel(self):
        ### get velocity command -> calculate velocity according to velocity profile -> publish velocity ### 
        self.update_params()
        self.calculate_profile(self.target_vel)
        self.vel_pub.publish(self.vel_msg)

    def calculate_profile(self, target_vel):
        ### simple trapezodial profile, velocity +- (acc*time)
        ### Linear ###
        if(target_vel.linear.x > self.vel_msg.linear.x ):
            self.vel_msg.linear.x = min(target_vel.linear.x, self.vel_msg.linear.x + (self.limit_lin_acc * self.time_step) )
        elif(target_vel.linear.x < self.vel_msg.linear.x ):
            self.vel_msg.linear.x = max(target_vel.linear.x, self.vel_msg.linear.x - (self.limit_lin_acc * self.time_step) )
        else:
            self.vel_msg.linear.x = target_vel.linear.x
        ### Angular ###
        if(target_vel.angular.z  > self.vel_msg.angular.z ):
            self.vel_msg.angular.z  = min(target_vel.angular.z , self.vel_msg.angular.z + (self.limit_ang_acc * self.time_step) )
        elif(target_vel.angular.z  < self.vel_msg.angular.z ):
            self.vel_msg.angular.z  = max(target_vel.angular.z , self.vel_msg.angular.z - (self.limit_ang_acc * self.time_step) )
        else:
            self.vel_msg.angular.z  = target_vel.angular.z 


    def update_params(self):
        self.limit_lin_safety_forward = rospy.get_param("/config/limit_lin_safety_forward", self.limit_lin_safety_forward)
        self.limit_lin_safety_backward = rospy.get_param("/config/limit_lin_safety_backward", self.limit_lin_safety_backward)
        self.limit_lin_vel = rospy.get_param("/config/limit_lin_vel", self.limit_lin_vel)
        self.limit_lin_acc = rospy.get_param("/config/limit_lin_acc", self.limit_lin_acc)
        self.limit_ang_vel = rospy.get_param("/config/limit_ang_vel", self.limit_ang_vel)
        self.limit_ang_acc = rospy.get_param("/config/limit_ang_acc", self.limit_ang_acc)


if(__name__=="__main__"):
    rospy.init_node("velocity_filter")
    vel = velocity_filter()
    rate = rospy.Rate(10)
    while( not rospy.is_shutdown()):
        if(not vel.brake):
            vel.update_vel()
        rate.sleep()
