#!/usr/bin/env python3

"""
no velocity smoothing. only keep velocity command rate in fixed rate.
"""

import rospy
from geometry_msgs.msg import Twist, TwistStamped
from std_msgs.msg import Bool

class velocity_filter:
    def __init__(self) -> None:
        self.time_step = 0.1
        self.previous_time = rospy.Time.now().to_sec()
        
        self.limit_lin_safety_forward = 0.5
        self.limit_lin_safety_backward = 0.5
        self.limit_lin_vel = 1.0
        self.limit_lin_acc = 1.0
        self.limit_ang_vel = 1.0
        self.limit_ang_acc = 1.0

        self.current_vel = Twist()
        self.target_vel = Twist()
        self.vel_msg = Twist()
        self.vel_pub = rospy.Publisher("cmd_vel", Twist, queue_size=10)
        self.target_vel_sub = rospy.Subscriber("mux_vel", Twist, self.target_vel_callback)
        self.emergency_brake_sub = rospy.Subscriber("joy_pause", Bool, self.brake_callback)
        self.brake = False

    def brake_callback(self, data):
        self.brake = data.data
        if(self.brake):
            self.reset_vel()
            self.vel_pub.publish(self.vel_msg)

    def reset_vel(self):
        self.current_vel = Twist()
        self.target_vel = Twist()
        self.vel_msg = Twist()


    def target_vel_callback(self, data):
        ### filter velocity to not exceed limit ###
        self.target_vel = data
        if(self.target_vel.linear.x >= 0):
            self.target_vel.linear.x = min(self.target_vel.linear.x, min( self.limit_lin_vel, self.limit_lin_safety_forward))
        elif(self.target_vel.linear.x < 0):
            self.target_vel.linear.x = max(self.target_vel.linear.x, -1 * min( self.limit_lin_vel, self.limit_lin_safety_backward))

        if(self.target_vel.angular.z >= 0):
            self.target_vel.angular.z = min(self.target_vel.angular.z, self.limit_ang_vel)
        elif(self.target_vel.angular.z < 0):
            self.target_vel.angular.z = max(self.target_vel.angular.z, -1*self.limit_ang_vel)

        

    def update_vel(self):
        ### get velocity command -> calculate velocity according to velocity profile -> publish velocity ### 
        self.vel_msg.linear.x = self.target_vel.linear.x
        self.vel_msg.angular.z  = self.target_vel.angular.z 
        self.vel_pub.publish(self.vel_msg)

if(__name__=="__main__"):
    rospy.init_node("velocity_filter")
    vel = velocity_filter()
    rate = rospy.Rate(10)
    while( not rospy.is_shutdown()):
        if(not vel.brake):
            vel.update_vel()
        rate.sleep()
