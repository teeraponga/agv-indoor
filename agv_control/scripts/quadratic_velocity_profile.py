#!/usr/bin/env python3

import rospy
from std_msgs.msg import Float32, UInt8, Bool
from sensor_msgs.msg import Joy
from geometry_msgs.msg import Twist


"""
Subscribe velocity from self.twist_mux and apply velocity profile.
Use S-curve velocity profile.
Calculate velocity curve using quadratic formula.
"""

class quadratic_velocity_filter:
    def __init__(self):

        self.twist = Twist()

        ### get velocity limit from rosparam
        self.max_linear_vel = rospy.get_param("/config/limit_lin_vel", 0.5)
        self.max_angular_vel = rospy.get_param("/config/limit_ang_vel", 0.5)

        ### get acceleration limit from rosparam
        self.max_linear_acc = rospy.get_param("/config/limit_lin_acc", 1.5)
        self.max_angular_acc = rospy.get_param("/config/limit_ang_acc", 9.0)

        self.acc_max = [self.max_linear_acc, 1.0, self.max_angular_acc] ### [max_acc_x, max_acc_y, max_acc_angular]

        #index list
        self.linear_x = 0
        self.linear_y = 1
        self.angular_z = 2

        ### joystick controller dead zone
        self.threshold = [0.064, 0.013, 0.001]   #linear_x, linear_y, angular_z

        #vel target
        self.cmd_vel = [0, 0, 0]                 #linear_x, linear_y, angular_z
        #pre vel target
        self.pre_value = [0, 0, 0]               #linear_x, linear_y, angular_z

        self.vel_now = [0, 0, 0]                 #linear_x, linear_y, angular_z

        self.acc_now = [0, 0, 0]                 #linear_x, linear_y, angular_z
        #tau = time now - current time stamp
        self.tau_use = [0, 0, 0]                 #linear_x, linear_y, angular_z
        #time
        self.T = [0, 0, 0]                       #linear_x, linear_y, angular_z

        #coef variable
        self.c_value = [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]]

        self.Hz_command = 20

        self.joy_emer_stop = False
        self.bumper_status = 0 # 0:free, 1:forward bumper, 2:backward bumper

        self.pub = rospy.Publisher('/cmd_vel', Twist, queue_size=10)
        self.pub_acc = rospy.Publisher('/acc', Float32, queue_size=10)

        self.sub = rospy.Subscriber('/mux_vel', Twist, self.callback)
        self.brake_sub = rospy.Subscriber('/joy_pause', Bool, self.brake_callback)

    """
    scale value from 1 unit to vel
    data : value from axes joy
    threshould : dead zone
    """
    def cal_vel(self, data, threshould):
        if data > threshould:
            vel = (data - threshould) / (1.0 - threshould)
        elif data < ((-1) * threshould):
            vel = (data + threshould) / (1.0 - threshould)
        else:
            vel = 0
        return vel

    """
    find coef
    vi = vel initial
    ai = acc initial
    vf = vel target
    af = acc target (0)
    T = time of traj
    """
    def coef(self, vi, ai, vf, af, T):
        c0 = vi
        c1 = ai
        c2 = ((3*(vf-vi))/(T**2))-((af+(2*ai))/T)
        c3 = -((2*(vf-vi))/(T**3))+((af+ai)/(T**2))
        return [c0, c1, c2, c3]

    def brake_callback(self, data):
        if(data.data):
            self.joy_emer_stop = True
            self.reset_params()
        else:
            self.joy_emer_stop = False

    def reset_params(self):
        self.c_value = [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]]
        self.T = [0, 0, 0]
        self.tau_use = [999, 999, 999]
        self.acc_now = [0, 0, 0]
        self.cmd_vel = [0, 0, 0]
        self.pre_value = [0, 0, 0]
        self.vel_now = [0, 0, 0]

    def callback(self, data = Twist()):
        
        """
        cal target vel from axes data
        """
        forward_factor = rospy.get_param("/config/forward_factor", 1.0)
        backward_factor = rospy.get_param("/config/backward_factor", 1.0)
        rotate_factor = rospy.get_param("/config/spin_factor", 1.0)
        # self.cmd_vel[linear_x] = cal_vel(axes[1], threshold[linear_x]) * max_linear_vel
        # self.cmd_vel[linear_y] = cal_vel(axes[0], threshold[linear_y]) * max_linear_vel
        # self.cmd_vel[angular_z] = cal_vel(axes[3], threshold[angular_z]) * max_angular_vel
        if(self.joy_emer_stop):
            return

        if(data.linear.x > 0):
            self.cmd_vel = [data.linear.x * forward_factor, data.linear.y, data.angular.z * rotate_factor]
        else:
            self.cmd_vel = [data.linear.x * backward_factor, data.linear.y, data.angular.z * rotate_factor]

        for axis in range(3):
            if self.cmd_vel[axis] != self.pre_value[axis]:
                self.pre_value[axis] = self.cmd_vel[axis]
                self.T[axis] = 0.1
                """
                loop increase time(T) for check limit acc
                """
                exitt = True
                while exitt:
                    exitt = False
                    self.c_value[axis] = self.coef(self.vel_now[axis], self.acc_now[axis], self.cmd_vel[axis], 0, self.T[axis])
                    for i in range(int(self.T[axis]*self.Hz_command)):
                        tau = i*(1/self.Hz_command)
                        acc = self.c_value[axis][1] + (2*self.c_value[axis][2]*tau) + (3*self.c_value[axis][3]*(tau**2))
                        if abs(acc) > self.acc_max[axis]:
                            self.T[axis] = self.T[axis] + 0.1
                            exitt = True
                            break
                self.tau_use[axis] = 0

if __name__ == '__main__':
    rospy.init_node('velocity_filter', anonymous=True)

    qvf = quadratic_velocity_filter()

    r = rospy.Rate(qvf.Hz_command)
    while(not rospy.is_shutdown()):
        if(not qvf.joy_emer_stop):
            for axis in range(3):
                if qvf.tau_use[axis] < qvf.T[axis]:
                    qvf.vel_now[axis] = qvf.c_value[axis][0] + (qvf.c_value[axis][1]*qvf.tau_use[axis]) + (qvf.c_value[axis][2]*(qvf.tau_use[axis]**2)) + (qvf.c_value[axis][3]*(qvf.tau_use[axis]**3))
                    qvf.acc_now[axis] = qvf.c_value[axis][1] + (2*qvf.c_value[axis][2]*qvf.tau_use[axis]) + (3*qvf.c_value[axis][3]*(qvf.tau_use[axis]**2))
                else:
                    qvf.vel_now[axis] = qvf.cmd_vel[axis]
                    qvf.acc_now[axis] = 0
                qvf.tau_use[axis] = qvf.tau_use[axis]+(1/qvf.Hz_command)
            qvf.twist.linear.x = qvf.vel_now[qvf.linear_x]
            qvf.twist.linear.y = qvf.vel_now[qvf.linear_y]
            qvf.twist.angular.z = qvf.vel_now[qvf.angular_z]
        else:
            qvf.twist.linear.x = 0
            qvf.twist.linear.y = 0
            qvf.twist.angular.z = 0
        qvf.pub.publish(qvf.twist)
        r.sleep()