#!/usr/bin/env python3

from smbus2 import SMBus
import time 

import numpy as np

import rospy
# import tf
from std_msgs.msg import Int16MultiArray
from sensor_msgs.msg import Range

"""
Listen to ultrasonic data and set limit safety velocity
"""

class ultrasonic_safety: 
    
    def __init__(self ) :
        self.ult_sub = rospy.Subscriber("ultrasonic_data", Int16MultiArray, self.ultrasonic_callback)
        
        # self.sensor_x_axis = 0.30 ## meter from robot's center
        self.previous_dis = 0

        ### Left ultrasonic ###
        self.ult1_msg = Range()
        self.ult1_pub = rospy.Publisher("ultrasonic1", Range, queue_size=10)
        self.ult1_msg.header.frame_id = "ult1_link"
        self.ult1_msg.field_of_view = 1
        self.ult1_msg.min_range = 0.03
        self.ult1_msg.max_range = 1.5
        ### Right ultrasonic ###
        self.ult2_msg = Range()
        self.ult2_pub = rospy.Publisher("ultrasonic2", Range, queue_size=10)
        self.ult2_msg.header.frame_id = "ult2_link"
        self.ult2_msg.field_of_view = 0.5
        self.ult2_msg.min_range = 0.03
        self.ult2_msg.max_range = 1.5
        
        # self.max_lin_vel = rospy.get_param("/config/limit_lin_vel", 0.5)
        self.interval_limit = [  ### [ultrasonic_range,  max_vel]
            [0.10, 0.0],
            [0.20, 0.05],
            [0.30, 0.1],
            [0.40, 0.1],
            [0.50, 0.15],
            [0.60, 0.15],
            [0.70, 0.2],
            [0.90, 0.25],
            [1.10, 0.3],
            [1.30, 0.4],
            [1.50, 0.5],
        ]

        # self.current_vel = Twist()
        # self.cmd_vel_sub = rospy.Subscriber("joy_vel", Twist, self.cmd_vel_callback)
        # self.vel_pub = rospy.Publisher("cmd_vel", Twist, queue_size=10)

    # def cmd_vel_callback(self,data):
    #     self.current_vel = data
    #     if(self.current_vel.linear.x >= 0):
    #         self.current_vel.linear.x = min(self.current_vel.linear.x, self.max_lin_vel)
    #     # else:
    #     #     self.current_vel.linear.x = -1 * min(abs(self.current_vel.linear.x), self.max_lin_vel)
    #     self.vel_pub.publish(self.current_vel)

    def ultrasonic_callback(self, data):
        ### use to adjust max linear velocity ###
        ### for this case ultranic sensors are in robot's front
        ##   [ middle_sensor, left_sensor, right_sensor ]   ##

        ### Publish to Range topic ###
        self.ult1_msg.header.stamp = self.ult2_msg.header.stamp = rospy.Time.now()
        self.ult1_msg.range = data.data[1]/100.0 ## convert to m
        self.ult2_msg.range = data.data[2]/100.0 ## convert to m
        self.ult1_pub.publish(self.ult1_msg)
        self.ult2_pub.publish(self.ult2_msg)
        ### ### ### ###

        min_ult = min(data.data[1:3])/100.0 ## find min value and convert to m 
        if(min_ult > self.interval_limit[-1][0]):
            rospy.set_param("/config/limit_lin_safety_forward", rospy.get_param("/config/limit_lin_vel", 0.5) )
            return
        for l in self.interval_limit:
            if(min_ult > l[0]):
                self.previous_dis = l[1]
                # self.max_lin_vel = l[1]
            else:
                # print(self.previous_dis)
                rospy.set_param("/config/limit_lin_safety_forward", self.previous_dis)
                break


data_hz = 10
num_sensors = 2
if(__name__=="__main__"):
    rospy.init_node("ultrasonic_safety")
    
    safety = ultrasonic_safety()

    rospy.spin()

    # rate = rospy.Rate(data_hz)
    # while(not rospy.is_shutdown()):
    #     pass