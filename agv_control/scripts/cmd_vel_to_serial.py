#!/usr/bin/env python3

import rospy
from geometry_msgs.msg import Twist
import serial
import time

from rospy.core import is_shutdown

class cmd_vel_to_serial:
    def __init__(self):
        self.sub = rospy.Subscriber("/cmd_vel",Twist,self.callback,queue_size=10)
        self.serial = serial.Serial(port='/dev/ttyUSB1', baudrate=115200)
        self.r = rospy.get_param("/config/radius",0.0325)
        self.w = rospy.get_param("/config/width",0.16)
        self.max_v = rospy.get_param("/config/limit_linear",1)
        self.max_w = rospy.get_param("/config/limit_angular",2)
        # self.d = rospy.get_param("/config/depth",)
        self.max_pwm = 255
        self.min_pwm = 0

    def callback(self,data):
        v = data.linear.x
        w = data.angular.z
        vl, vr = self.kinematics(v,w)
        write_serial = self.serial_mapping(vr,vl)
        self.write_serial(write_serial)
        # print(write_serial)


    def write_serial(self,data):
        self.serial.write(bytes(data, 'utf-8'))
        time.sleep(0.05)
        # data = self.serial.readline()
        # return data
        
    def kinematics(self,v,w):
        # R = (v + (self.w*w)/2) / self.r
        # L = (v - (self.w*w)/2) / self.r
        R = (v + (self.w*w*5))
        L = (v - (self.w*w*5))
        return L, R

    def serial_mapping(self,vr,vl):
        # return "L+040NR-040E"
        l = int((vl * 215.79) )#- 27.68)
        r = int((vr * 215.79) )#- 27.68)
        pwm_l = format( abs(l) , '03d' )
        pwm_r = format( abs(r) , '03d' )
        if(l>=0):
            pwm_l = f"+{pwm_l}"
        else:
            pwm_l = f"-{pwm_l}"
        if(r>=0):
            pwm_r = f"+{pwm_r}"
        else:
            pwm_r = f"-{pwm_r}"
        return f"L{pwm_l}NR{pwm_r}E"

if(__name__=="__main__"):
    rospy.init_node("vel_to_serial")
    vel_to_serial = cmd_vel_to_serial()
    rate = rospy.Rate(10)
    while(not rospy.is_shutdown()):
        rate.sleep()