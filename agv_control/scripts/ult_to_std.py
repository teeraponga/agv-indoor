#!/usr/bin/env python3

""" liten to ultrasonic(Int16MultiArray) and publish to Int16 topics"""

import rospy
from rospy.core import is_shutdown
from std_msgs.msg import Int16,Int16MultiArray

topic = "/ultrasonic_data"

pub_0 = rospy.Publisher("mid", Int16, queue_size=10)
pub_1 = rospy.Publisher("left", Int16, queue_size=10)
pub_2 = rospy.Publisher("right", Int16, queue_size=10)
msg0 = Int16()
msg1 = Int16()
msg2 = Int16()

def callback(data):
    msg0.data = max(data.data[0], -1)
    msg1.data = max(data.data[1], -1)
    msg2.data = max(data.data[2], -1)
    pub_0.publish(msg0)
    pub_1.publish(msg1)
    pub_2.publish(msg2)
    pass

if(__name__=="__main__"):
    rospy.init_node("plot_ult")
    sub = rospy.Subscriber(topic, Int16MultiArray, callback=callback)
    rospy.spin()