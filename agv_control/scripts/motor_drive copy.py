#!/usr/bin/env python3

"""
Drive 2 wheels diff drive robot with topic cmd_vel.
This Program subscribe cmd_vel topic from ROS, then calculate RPM for each wheel.
Then send command to driver via RS485
"""

import rospy
import math
import time
from geometry_msgs.msg import Twist, TwistStamped
from rospy.core import is_shutdown
from std_msgs.msg import String
from rs485_communicator import Serial_communicator

class robot_driver:
    def __init__(self, control_rate=50) -> None:
        self.sub = rospy.Subscriber("/cmd_vel", Twist, self.callback_vel, queue_size=10)
        self.pub = rospy.Publisher("/rpm", Twist, queue_size=5)
        self.pub_msg = Twist()

        self.sub_cmd = rospy.Subscriber("/motor_cmd", String, self.callback_motor_command)

        self.feedback_vel = rospy.Publisher("/feedback_vel", TwistStamped, queue_size=10)

        ### robot params ###
        self.wheel_sep = 0.395 ## wheel seperation
        self.wheel_rad = 0.07 ## wheel radius
        ### ### ### ###

        self.comm = Serial_communicator(control_rate=control_rate)

        self.vel_msg = TwistStamped()


    def callback_vel(self,data):
        """
        callback from cmd_vel
        1. get velocity from Twist topic.
        2. calculate rpm for each wheel.
        3. send command through serial (dont need receive return for now)
        """
        L, R = self.kinematic( data.linear.x, data.angular.z )
        # print(L)
        # print(R)
        self.comm.send_rpm(1, L)
        # time.sleep(0.01)
        self.comm.send_rpm(2, R)
        # time.sleep(0.01)
        self.pub_msg.linear.x = L
        self.pub_msg.linear.y = R
        self.pub.publish(self.pub_msg)

    def callback_motor_command(self,data):
        
        if(data.data=='s'):
            print(0)
            self.comm.send_mode()
        elif(data.data=='r'):
            print(1)
            self.comm.send_enable()
        elif(data.data=='c'):
            print(10)
            self.comm.send_clear()
        else:
            print(2)
            self.comm.send_disable()


    def kinematic(self,v,w):
        """2 wheels diff drive kinematic from v and w to rpm"""
        R = ((v + (self.wheel_sep*w/2))/self.wheel_rad) * (60/(2*math.pi))  ## calculate RPM frorm velocity and omega
        L = (-1) * ((v - (self.wheel_sep*w/2))/self.wheel_rad) * (60/(2*math.pi))  ## calculate RPM frorm velocity and omega ## * -1 to fix heading
        return int(L), int(R)

    def inverse_kinematic(self,l,r):
        L = (l * 2 * math.pi * self.wheel_rad) / 60
        R = (r * 2 * math.pi * self.wheel_rad) / 60
        self.vel_msg.twist.linear.x = (L + R) / 2
        self.vel_msg.twist.angular.z = (R - L) / self.wheel_sep

        
if(__name__=="__main__"):
    rospy.init_node('test_drive')
    control_rate = 200 ## Hz
    feedback_vel_rate = rospy.get_param("feedback_vel_freq", default=10)  #10 ##hz
    
    r = robot_driver(control_rate=control_rate)
    rate = rospy.Rate(control_rate)
    count = 1

    msg = String()
    msg.data = 's'

    r.callback_motor_command(msg)
    msg.data = 'r'
    time.sleep(0.5)
    r.callback_motor_command(msg)
    time.sleep(0.5)

    while(not rospy.is_shutdown()):
        count+=1
        r.comm.send_message_queue()
        if(count==int(control_rate/feedback_vel_rate)):
            count=1
            r.comm.read_vel()
            r.inverse_kinematic(r.comm.left_rpm, r.comm.right_rpm)
            r.vel_msg.header.stamp = rospy.Time.now()
            r.feedback_vel.publish(r.vel_msg)
            # print(r.comm.left_rpm)
            # print(r.comm.right_rpm)
            # print("-----")
        # print(r.comm.msg_queue.qsize())    
        rate.sleep()