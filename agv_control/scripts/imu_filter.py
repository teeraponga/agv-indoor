#!/usr/bin/env python3

"""
Filter imu value to assume 2d flatworld with gravity always pointed downward
"""

from sensor_msgs.msg import Imu
import rospy

class imu_filter:
    def __init__(self):
        self.sub = rospy.Subscriber("imu_gz",Imu,callback=self.callback)
        self.pub = rospy.Publisher("imu_filter", Imu, queue_size=30)
        self.filtered = Imu()

    def callback(self,data):
        self.filtered = Imu()
        self.filtered = data
        ### orientation in quaternion 
        self.filtered.orientation.x = 0
        self.filtered.orientation.y = 0
        self.filtered.angular_velocity.x = 0
        self.filtered.angular_velocity.y = 0
        self.filtered.linear_acceleration.z = 9.8
        self.pub.publish(self.filtered)


if(__name__=="__main__"):
    rospy.init_node("imu_filter_node")
    imufilter = imu_filter()
    rospy.spin()
    
