#!/usr/bin/env python3
import rospy
from std_msgs.msg import Bool, String
from geometry_msgs.msg import Twist
from sensor_msgs.msg import Joy
import argparse


msg = """
Control Your Robot!
---------------------------
Moving around by Analog (Don't forget enable MODE on joystick) :
    Linear -- Left Analog
    Angular -- Right Analog
    RB -- Emergency Brake
    Y -- Increase Max Linear Velocity
    A -- Decrease Max Linear Velocity
    X -- Increase Max Angular Velocity
    B -- Decrease Max Angular Velocity
CTRL-C to quit
"""

e = """
Communications Failed
"""


class joy2vel():

    def __init__(self):

        self.omni = rospy.get_param("/config/omni", default=False)
        self.config = rospy.get_param("~keymap")
        self.LIMIT_LIN_VEL = rospy.get_param("/config/limit_lin_vel", 1.0)
        self.LIMIT_ANG_VEL = rospy.get_param("/config/limit_ang_vel", 1.8)
        self.LIN_VEL_STEP_SIZE = self.LIMIT_LIN_VEL / 4.0
        self.ANG_VEL_STEP_SIZE = self.LIMIT_ANG_VEL / 4.0
        self.LIN_VEL = self.LIMIT_LIN_VEL / 2.0
        self.ANG_VEL = self.LIMIT_ANG_VEL / 2.0
        self.SEQ = 0

        self.PREVIOUS_LIN_VEL = 0
        self.PREVIOUS_ANG_VEL = 0

        self.sub_joy = rospy.Subscriber('joy', Joy, self.callback)
        self.pub = rospy.Publisher("/obstacle_bot/cmd_vel", Twist, queue_size=10)
        self.brake_pub = rospy.Publisher("/joy_pause", Bool, queue_size=10)

        self.twist = Twist()
        self.emergency_brake = False
        self.brake_msg = Bool()

        self.vel_changed = False

        ### manual push mode ###
        self.push_button_clicked = False
        self.is_push_mode = False
        self.mode_pub = rospy.Publisher("/motor_cmd", String, queue_size=10)

    def callback(self, data):
        self.joy_buttons = data.buttons
        self.joy_axes = data.axes

        if data.buttons[self.config["brake"]] == 1 :
            if(not self.emergency_brake):
                self.reset()
                self.brake_msg.data = self.emergency_brake = True
                rospy.set_param("/config/emergency_brake", True)
            self.brake_pub.publish(self.brake_msg)
            return 0
        elif data.buttons[self.config["brake"]] == 0 and self.emergency_brake:
            self.brake_msg.data = self.emergency_brake = False
            rospy.set_param("/config/emergency_brake", False)
            self.brake_pub.publish(self.brake_msg)

        if(data.buttons[self.config["push_mode"]] == 1 and not self.push_button_clicked):
            self.push_button_clicked = True
            self.is_push_mode = not self.is_push_mode
            if(self.is_push_mode):
                self.mode_pub.publish(String(""))
            else:
                self.mode_pub.publish(String("r"))
        elif(data.buttons[self.config["push_mode"]] == 0 ):
            self.push_button_clicked = False

        if not self.emergency_brake:
            if data.buttons[self.config["lin_speed_up"]] == 1 and self.SEQ != data.header.seq and not self.vel_changed:
                self.LIN_VEL += self.LIN_VEL_STEP_SIZE
                if self.LIN_VEL > self.LIMIT_LIN_VEL:
                    self.LIN_VEL = self.LIMIT_LIN_VEL
                self.vel_changed = True

            elif data.buttons[self.config["lin_speed_down"]] == 1 and self.SEQ != data.header.seq and not self.vel_changed:
                self.LIN_VEL -= self.LIN_VEL_STEP_SIZE
                if self.LIN_VEL < 0:
                    self.LIN_VEL = 0
                self.vel_changed = True

            elif data.buttons[self.config["ang_speed_up"]] == 1 and self.SEQ != data.header.seq and not self.vel_changed:
                self.ANG_VEL += self.ANG_VEL_STEP_SIZE
                if self.ANG_VEL > self.LIMIT_ANG_VEL:
                    self.ANG_VEL = self.LIMIT_ANG_VEL
                self.vel_changed = True

            elif data.buttons[self.config["ang_speed_down"]] == 1 and self.SEQ != data.header.seq and not self.vel_changed:
                self.ANG_VEL -= self.ANG_VEL_STEP_SIZE
                if self.ANG_VEL < 0:
                    self.ANG_VEL = 0
                self.vel_changed = True
            
            elif data.buttons[self.config["lin_speed_up"]] == 0 and data.buttons[self.config["lin_speed_down"]] == 0 and data.buttons[self.config["ang_speed_up"]] == 0 and data.buttons[self.config["ang_speed_down"]] == 0:
                self.vel_changed = False
 
            # elif data.buttons[self.config["reset"]] == 1 and self.SEQ != data.header.seq:
            #     self.LIN_VEL = self.LIMIT_LIN_VEL / 2.0
            #     self.ANG_VEL = self.LIMIT_ANG_VEL / 2.0
            # else:
            #     self.LIN_VEL = rospy.get_param("~max_lin_vel")
            #     self.ANG_VEL = rospy.get_param("~max_ang_vel")

            vel_x = data.axes[self.config["lin_vel_x"]]
            if abs(vel_x) < 0.2:
                vel_x = 0.0
            self.twist.linear.x = vel_x * self.LIN_VEL
            if self.omni:
                vel_y = -data.axes[self.config["lin_vel_y"]]
                if abs(vel_y) < 0.2:
                    vel_y = 0.0
                self.twist.linear.y = vel_y * self.LIN_VEL
            ang_z = data.axes[self.config["ang_vel_z"]]
            if abs(ang_z) < 0.2:
                ang_z = 0.0
            self.twist.angular.z = ang_z * self.ANG_VEL
            self.SEQ = data.header.seq
            self.LIMIT_LIN_VEL = rospy.get_param("/config/limit_lin_vel", 1.0)
            self.LIMIT_ANG_VEL = rospy.get_param("/config/limit_ang_vel", 2.0)
            self.LIN_VEL_STEP_SIZE = self.LIMIT_LIN_VEL / 4.0
            self.ANG_VEL_STEP_SIZE = self.LIMIT_ANG_VEL / 4.0

            if(data.buttons[self.config["turbo"]] == 1):
                self.twist.linear.x *= 2.0
                self.twist.angular.z *= 2.0

            if(self.PREVIOUS_LIN_VEL == 0 and self.PREVIOUS_ANG_VEL == 0 and self.twist.linear.x==0 and self.twist.angular.z==0):
                return

            self.PREVIOUS_LIN_VEL = self.twist.linear.x
            self.PREVIOUS_ANG_VEL = self.twist.angular.z

            self.pub.publish(self.twist)

    def reset(self):
        self.twist = Twist()
        self.pub.publish(self.twist)

    def check_press_button(self):
        joy_buttons = list(self.joy_buttons)
        joy_axes = list(self.joy_axes)
        other_button = joy_buttons + joy_axes
        print(other_button)

        result = False
        if len(other_button) > 0 :
            result = all(elem == other_button[0] for elem in other_button)
        if result :
            print("All Elements in List are Equal")
            self.press_other_button = False
        else:        
            print("All Elements in List are Not Equal")
            self.press_other_button = True


if __name__ == "__main__":

    rospy.init_node('teleop_joy_to_velocity')
    joycmd = joy2vel()
    try:
        print(msg)
        rospy.spin()

    except BaseException:
        print(e)

    finally:
        joycmd.reset()
