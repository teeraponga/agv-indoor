#!/usr/bin/env python3

from smbus2 import SMBus
import time 

import numpy as np

import rospy
from rospy.core import is_shutdown
from std_msgs.msg import Int16MultiArray

"""
Read ultrasonic sensor and publish as Int16MultiArray
"""

bus_number = 1

class ultrasonic_sensor: 
    _bus = SMBus(bus_number)

    _max_range = 600 ## max ultrasonic sensor (cm)

    _distance_data_id = 3  ## register id of measured distance
    _mode_reg_id = 7  ## registere id to config mode of opration
    _config_reg_id = 8  ## register id to send command to (e.g. read value command)

    _mode_data = 0x00  ## passive mode, range 150cm
    _read_distance_data = 0x01 ## command to read distance from sensor

    def __init__(self, address=0x11) -> None:
        self.address = address
        self._bus.write_byte_data(self.address, self._mode_reg_id, self._mode_data)  ## set mode of ultrasonic sensor
        time.sleep(0.1)
        
        self.distance = 999

    def send_read_command(self):
        self._bus.write_byte_data(self.address, self._config_reg_id, self._read_distance_data)
        time.sleep(0.01)
        
    def read_data(self):
        block = self._bus.read_i2c_block_data(self.address, self._distance_data_id, 2)
        self.distance = block[0]*255 + block[1]


data_hz = 10
num_sensors = 2
if(__name__=="__main__"):
    rospy.init_node("test_ultrasonic")
    pub = rospy.Publisher("ultrasonic_data", Int16MultiArray, queue_size=10)
    pub_msg = Int16MultiArray()

    ### Try connect to i2c every second    
    timeout_count = 0
    timeout_max = 30
    while(True):
        try:
            ult0 = ultrasonic_sensor(0x11) #Middle
            ult1 = ultrasonic_sensor(0x12) #Left
            ult2 = ultrasonic_sensor(0x13) #Right
            break
        except OSError:
            timeout_count+=1
            if(timeout_count==timeout_max):
                print("Timed out, cannot connect to I2C...")
                exit()
            else:
                time.sleep(1)
    ### ### ###

    rate = rospy.Rate(data_hz)
    while(not rospy.is_shutdown()):
        try:
            ult0.send_read_command()
            ult1.send_read_command()
            ult2.send_read_command()
            rate.sleep()
            ult0.read_data()
            ult1.read_data()
            ult2.read_data()
            pub_msg.data = [abs(np.int16(ult0.distance)), abs(np.int16(ult1.distance)), abs(np.int16(ult2.distance))]
            pub.publish(pub_msg)
        except OSError:
            continue

        # print(str.format("1: {0}, 2: {1}", ult1.distance, ult2.distance))