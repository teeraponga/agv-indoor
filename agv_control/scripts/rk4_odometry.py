#!/usr/bin/env python3
import rospy
from geometry_msgs.msg import Twist, TwistStamped, TransformStamped
from nav_msgs.msg import Odometry
from tf.transformations import quaternion_from_euler, euler_from_quaternion
from tf import TransformBroadcaster
from std_msgs.msg import Byte
import numpy as np


class State2D:
    def __init__(self):
        self.x = 0.0
        self.y = 0.0
        self.theta = 0.0


"""
calculate robot odometry from robot's feedback velocity.
rk4 is a method to filter noise in linear position.
TODO: Find way to filter theta as well. Make wheel odom more accurate.
"""
class RK4Odometry:
    def __init__(self, odom_frame="odom_rk4", child_frame="base_footprint"):
        rospy.loginfo("Starting RK4 Odometry builder.")
        rospy.Subscriber("/feedback_vel", TwistStamped, self.feedback_callback)

        self.tf_stamaped = TransformStamped()
        self.tf_broadcaster = TransformBroadcaster()
        self.tf_stamaped.header.frame_id = odom_frame
        self.tf_stamaped.child_frame_id = child_frame

        self.odom_pub = rospy.Publisher("/wheel_odometry", Odometry, queue_size=5)
        # self.freq = rospy.get_param("feedback_vel_freq", default=10)
        self.previous_time = None
        self.Dt = 0 #1.0 / self.freq
        self.state = State2D()
        self.odom = Odometry()
        self.odom.header.frame_id = odom_frame
        self.odom.child_frame_id = child_frame
        self.odom.pose.covariance = [1e-5, 0, 0, 0, 0, 0,
                                     0, 1e-5, 0, 0, 0, 0,
                                     0, 0, 1e12, 0, 0, 0,
                                     0, 0, 0, 1e12, 0, 0,
                                     0, 0, 0, 0, 1e12, 0,
                                     0, 0, 0, 0, 0, 1e-3]
        self.odom.twist.covariance = [0, 0, 0, 0, 0, 0,
                                      0, 0, 0, 0, 0, 0,
                                      0, 0, 0, 0, 0, 0,
                                      0, 0, 0, 0, 0, 0,
                                      0, 0, 0, 0, 0, 0,
                                      0, 0, 0, 0, 0, 0]

        self.emer_pressed = False

    def feedback_callback(self, data=TwistStamped()):
        if(self.previous_time is None):
            self.previous_time = data.header.stamp
            return
        else:
            self.Dt = (data.header.stamp - self.previous_time).to_sec()
            self.previous_time = data.header.stamp 
        new_s = self.rk4_integrate(data.twist.linear.x, data.twist.angular.z)
        self.state.x = new_s[0]
        self.state.y = new_s[1]
        self.state.theta = new_s[2]
        q = quaternion_from_euler(0, 0, self.state.theta)

        #### tf message ####
        self.tf_stamaped.header.stamp = rospy.Time.now()
        self.tf_stamaped.transform.translation.x = self.state.x
        self.tf_stamaped.transform.translation.y = self.state.y
        self.tf_stamaped.transform.rotation.x = q[0]
        self.tf_stamaped.transform.rotation.y = q[1]
        self.tf_stamaped.transform.rotation.z = q[2]
        self.tf_stamaped.transform.rotation.w = q[3]

        self.tf_broadcaster.sendTransformMessage(self.tf_stamaped)
        #### #### #### ####

        ### odom message ###
        self.odom.header.stamp = rospy.Time.now()
        self.odom.pose.pose.position.x = self.state.x
        self.odom.pose.pose.position.y = self.state.y
        self.odom.pose.pose.orientation.x = q[0]
        self.odom.pose.pose.orientation.y = q[1]
        self.odom.pose.pose.orientation.z = q[2]
        self.odom.pose.pose.orientation.w = q[3]

        self.odom.twist.twist.linear.x = data.twist.linear.x
        self.odom.twist.twist.angular.z = data.twist.angular.z

        self.odom_pub.publish(self.odom)
        ### ### ### ### ###

    def kinematics(self, s, u):
        """
        x = s[0]
        y = s[1]
        theta = s[2]

        v = u[0]
        w = u[1]
        """

        d_x = np.cos(s[2]) * u[0]
        d_y = np.sin(s[2]) * u[0]
        d_theta = u[1]
        return np.array([d_x, d_y, d_theta])

    ### 
    def rk4_integrate(self, v, w):
        s = np.array([self.state.x, self.state.y, self.state.theta])
        u = np.array([v, w])
        k1 = self.kinematics(s, u)
        k2 = self.kinematics(s + k1 * 0.5 * self.Dt, u)
        k3 = self.kinematics(s + k2 * 0.5 * self.Dt, u)
        k4 = self.kinematics(s + k3 * self.Dt, u)
        d_s = (k1 + (2.0 * k2) + (2.0 * k3) + k4) / 6.0
        new_s = s + d_s * self.Dt
        return new_s

if __name__ == "__main__":
    rospy.init_node("odom_node")
    rk4_odom = RK4Odometry()
    rospy.spin()
