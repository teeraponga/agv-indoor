#!/usr/bin/env python3

"""
Drive 2 wheels diff drive robot with topic cmd_vel.
This Program subscribe cmd_vel topic from ROS, then calculate RPM for each wheel.
Then send command to driver via RS485
"""

import rospy
import math
import time
from geometry_msgs.msg import Twist, TwistStamped
from rospy.core import is_shutdown
from std_msgs.msg import String, Bool
from rs485_communicator import Serial_communicator

class robot_driver:
    def __init__(self, control_rate=50) -> None:
        self.sub = rospy.Subscriber("/cmd_vel", Twist, self.callback_vel, queue_size=10)
        self.pub = rospy.Publisher("/rpm", Twist, queue_size=5)
        self.pub_msg = Twist()

        self.sub_cmd = rospy.Subscriber("/motor_cmd", String, self.callback_motor_command)
        self.app_cmd = rospy.Subscriber("/push_btn", Bool, self.callback_app_command)
        self.app_push = False

        self.feedback_vel = rospy.Publisher("/feedback_vel", TwistStamped, queue_size=10)
        self.feedback_rpm = rospy.Publisher("/feedback_rpm", Twist, queue_size=10)

        ### robot params ###
        # self.wheel_sep = 0.395 ## wheel seperation
        self.wheel_sep = 0.44 ## wheel seperation
        self.wheel_rad = 0.07 ## wheel radius
        ### ### ### ###

        self.comm = Serial_communicator(control_rate=control_rate)

        self.vel_msg = TwistStamped()
        self.rpm_msg = Twist()

        ### Hard filter for rpm command ###
        ## Case#1 Use to avoid exact 0 rpm except brake. (prevent robot to jiggle when cross between + and - velocity) ##
        self.zero_count = 0
        self.zero_count_max = 3

        ## Case#2 Prevent angular velocity peak when move linearly while rotating ##
        self.previous_rpm_l = 0
        self.previous_rpm_r = 0
        self.l_cross = False
        self.r_cross = False
        self.cross_count = 0
        self.cross_max = 0
        self.increment_step = 0

    def callback_vel(self,data):
        """
        callback from cmd_vel
        1. get velocity from Twist topic.
        2. calculate rpm for each wheel.
        3. send command through serial (dont need receive return for now)
        """
        L, R = self.kinematic( data.linear.x, data.angular.z )
        
        ### Hard filter case 1 ###
        ## If receive 0 rpm,do not send it but just make a count.
        ## Once a count exceed max count, send command 0 rpm
        if(L==0 or R==0):
            if(self.zero_count<self.zero_count_max):
                self.zero_count+=1
                return
        else:
            self.zero_count = 0
        ### ### ###

        ### Hard filter  case 2 ###
        ## When only 1 side's rpm change direction, smoother the other side's rpm
        if(not self.l_cross and not self.r_cross):
            if(L * self.previous_rpm_l < 0 and R * self.previous_rpm_r > 0):
                # self.increment_step = ((R - self.previous_rpm_r)/self.cross_max)
                # print(self.increment_step)
                self.l_cross = True
                R = self.previous_rpm_r
            elif(L * self.previous_rpm_l > 0 and R * self.previous_rpm_r < 0):
                # self.increment_step = ((L - self.previous_rpm_l)/self.cross_max)
                # print(self.increment_step)
                self.r_cross = True
                L = self.previous_rpm_l
        elif(self.l_cross):
            self.cross_count+=1
            R = self.previous_rpm_r# + self.increment_step
        elif(self.r_cross):
            self.cross_count+=1
            L = self.previous_rpm_l #+ self.increment_step
        if(self.cross_count>=self.cross_max):
            self.l_cross = False
            self.r_cross = False
            self.cross_count = 0

        self.previous_rpm_r = R
        self.previous_rpm_l = L
        # print(L)
        # print(R)
        self.comm.send_rpm(1, L)
        # time.sleep(0.01)
        self.comm.send_rpm(2, R)
        # time.sleep(0.01)
        self.pub_msg.linear.x = L
        self.pub_msg.linear.y = R
        self.pub.publish(self.pub_msg)

    def callback_app_command(self,data):
        """
        disable/enable motor
        """
        if(data.data):
            if(self.app_push):
                self.comm.send_disable()
            else:
                self.comm.send_enable()
            self.app_push = not self.app_push

    def callback_motor_command(self,data):
        if(data.data=='s'):
            print(0)
            self.comm.send_mode()
        elif(data.data=='r'):
            print(1)
            self.comm.send_enable()
        elif(data.data=='c'):
            print(10)
            self.comm.send_clear()
        else:
            print(2)
            self.comm.send_disable()


    def kinematic(self,v,w):
        """2 wheels diff drive kinematic from v and w to rpm"""
        R = ((v + (self.wheel_sep*w/2))/self.wheel_rad) * (60/(2*math.pi))  ## calculate RPM frorm velocity and omega
        L = (-1) * ((v - (self.wheel_sep*w/2))/self.wheel_rad) * (60/(2*math.pi))  ## calculate RPM frorm velocity and omega ## * -1 to fix heading
        return int(L), int(R)

    def inverse_kinematic(self,l,r):
        """get robots linear/angular velocity from feedback rpm"""
        L = (l * 2 * math.pi * self.wheel_rad) / 60
        R = (r * 2 * math.pi * self.wheel_rad) / 60
        self.vel_msg.twist.linear.x = (L + R) / 2
        self.vel_msg.twist.angular.z = (R - L) / self.wheel_sep

        
if(__name__=="__main__"):
    rospy.init_node('test_drive')
    control_rate = 200 ## Hz
    feedback_vel_rate = rospy.get_param("feedback_vel_freq", default=20)  #10 ##hz
    
    r = robot_driver(control_rate=control_rate)
    rate = rospy.Rate(control_rate)
    count = 1

    msg = String()
    msg.data = 's'

    r.callback_motor_command(msg)
    msg.data = 'r'
    time.sleep(0.5)
    r.callback_motor_command(msg)
    time.sleep(0.5)

    while(not rospy.is_shutdown()):
        count+=1
        if(count==int(control_rate/feedback_vel_rate)):
            count=1
            r.comm.read_vel()
            r.inverse_kinematic(r.comm.left_rpm, r.comm.right_rpm)
            r.vel_msg.header.stamp = rospy.Time.now()
            # r.vel_msg.twist.linear.x *= 20
            # r.vel_msg.twist.angular.z *= 20
            r.feedback_vel.publish(r.vel_msg)

            r.rpm_msg.linear.x = r.comm.left_rpm*-1
            r.rpm_msg.linear.y = r.comm.right_rpm
            r.feedback_rpm.publish(r.rpm_msg)
            # print(r.comm.left_rpm)
            # print(r.comm.right_rpm)
            # print("-----")
        else:
            r.comm.send_message_queue()
        # print(r.comm.msg_queue.qsize())    
        rate.sleep()