#!/usr/bin/env python3

"""
Handle communication bewteen python and RS485 using serial
will handle message create, crc calculation, etc.
"""

import serial
import time

import queue

class Serial_communicator:
    def __init__(self, serial_port='/dev/rs485',baud=115200, control_rate=50) -> None:
        self.ser = serial.Serial(
            port=serial_port, 
            baudrate = baud,
            parity=serial.PARITY_NONE,
            stopbits=serial.STOPBITS_ONE,
            bytesize=serial.EIGHTBITS,
            timeout=0.1
        )
        
        self.left_rpm = 0
        self.right_rpm = 0

        self.msg_queue = queue.Queue(maxsize=int(control_rate/2)) # store queue of message
        self.msg_return = queue.Queue(maxsize=int(control_rate/2)) # store return byte lenght of each message


    def crc16(self, data: bytes, poly: hex = 0xA001) -> str:
        '''
            CRC-16 MODBUS HASHING ALGORITHM
        '''
        crc = 0xFFFF
        for byte in data:
            crc ^= byte
            for _ in range(8):
                crc = ((crc >> 1) ^ poly
                    if (crc & 0x0001)
                    else crc >> 1)

        return crc.to_bytes(2,'little')

    def send_message_queue(self):
        """
            Send messae in queue and read return byte
            for return byte only interested in feedback rpm (return byte = 7)
        """
        if(not self.msg_queue.empty()):
            msg = self.msg_queue.get()
            length = self.msg_return.get()
            self.send_message(msg)
            time.sleep(0.005)
            ret = self.read_message(length)
            if(length!=0 and len(ret)==0):
                print("No response..")
            elif(length==7 and len(ret)==7):
                if(ret.hex()[1]=='1'):
                    self.left_rpm = int.from_bytes( ret[3:5], 'big', signed=True) * -0.1
                elif(ret.hex()[1]=='2'):
                    self.right_rpm =  int.from_bytes( ret[3:5], 'big', signed=True) * 0.1

            # return ret

    def send_rpm(self, address, rpm):
        msg = []
        msg.append(address)
        msg.append(0x06)
        msg.append(0x20)
        msg.append(0x3A)
        h = self.tohex(rpm,16)
        msg.append(h[0])
        msg.append(h[1])
        c = self.crc16(msg)
        msg.append(c[0])
        msg.append(c[1])
        self.msg_queue.put(msg)
        self.msg_return.put(8)
        # self.send_message(msg)
        # print(msg)

    def send_mode(self):
        """ set motor to RUN mode """
        msg1 = [0x01, 0x06, 0x20, 0x32, 0x00, 0x03, 0x63, 0xC4]
        self.msg_queue.put(msg1)
        self.msg_return.put(8)
        # self.send_message(msg1)
        # time.sleep(0.02)
        # self.read_message(8)
        msg2 = [0x02, 0x06, 0x20, 0x32, 0x00, 0x03, 0x63, 0xF7]
        self.msg_queue.put(msg2)
        self.msg_return.put(8)
        # self.send_message(msg2)
        # time.sleep(0.02)
        # self.read_message(8)
        
    def send_enable(self):
        """ set motor to ENABLE """
        msg1 = [0x01, 0x06, 0x20, 0x31, 0x00, 0x08, 0xD2, 0x03]
        self.msg_queue.put(msg1)
        self.msg_return.put(8)
        # self.send_message(msg1)
        # time.sleep(0.02)
        # self.read_message(8)
        msg2 = [0x02, 0x06, 0x20, 0x31, 0x00, 0x08, 0xD2, 0x30]
        self.msg_queue.put(msg2)
        self.msg_return.put(8)
        # self.send_message(msg2)
        # time.sleep(0.02)
        # self.read_message(8)

    def send_disable(self):
        """ set motor to DISABLE """
        msg1 = [0x01, 0x06, 0x20, 0x31, 0x00, 0x07, 0x92, 0x07]
        self.msg_queue.put(msg1)
        self.msg_return.put(8)
        # self.send_message(msg1)
        # time.sleep(0.02)
        # self.read_message(8)
        msg2 = [0x02, 0x06, 0x20, 0x31, 0x00, 0x07, 0x92, 0x34]
        self.msg_queue.put(msg2)
        self.msg_return.put(8)
        # self.send_message(msg2)
        # time.sleep(0.02)
        # self.read_message(8)

    def send_clear(self):
        """ clear motor alarm """
        msg1 = [0x01, 0x06, 0x20, 0x31, 0x00, 0x06, 0x53, 0xC7]
        self.msg_queue.put(msg1)
        self.msg_return.put(8)
        # self.send_message(msg1)
        # time.sleep(0.02)
        # self.read_message(8)
        msg2 = [0x02, 0x06, 0x20, 0x31, 0x00, 0x06, 0x53, 0xF4]
        self.msg_queue.put(msg2)
        self.msg_return.put(8)
        # self.send_message(msg2)
        # time.sleep(0.02)
        # self.read_message(8)

    # def read_vel(self):
    #     if(self.msg_queue.qsize()>=self.msg_queue.maxsize-2):
    #         return
    #     msg1 = [0x01, 0x03, 0x20, 0x2C, 0x00, 0x01, 0x4E, 0x03]
    #     # self.ser.flushInput()
    #     self.msg_queue.put(msg1)
    #     self.msg_return.put(7)
    #     # self.send_message(msg1)
    #     # self.ser.flushOutput()
    #     # time.sleep(0.005)
    #     # self.right_rpm = self.read_message(7)
    #     time.sleep(0.005)

    #     msg2 = [0x02, 0x03, 0x20, 0x2C, 0x00, 0x01, 0x4E, 0x30]
    #     # self.ser.flushInput()
    #     self.msg_queue.put(msg2)
    #     self.msg_return.put(7)
    #     # self.send_message(msg2)
    #     # self.ser.flushOutput()
    #     # time.sleep(0.005)
    #     # self.left_rpm = self.read_message(7)
    #     # time.sleep(0.005)

    #     # if(self.right_rpm.hex()[3]==self.left_rpm.hex()[3]=='3'):
    #     #     print(self.right_rpm.hex())
    #     #     print(self.left_rpm.hex())
    #     #     print("------")

    def read_vel(self):
        """ read motor rpm """
        if(self.msg_queue.qsize()>=self.msg_queue.maxsize-2):
            return
        msg1 = [0x01, 0x03, 0x20, 0x2C, 0x00, 0x01, 0x4E, 0x03]
        self.ser.flushInput()
        self.send_message(msg1)
        self.ser.flushOutput()
        time.sleep(0.005)
        ret = self.read_message(7)
        time.sleep(0.005)
        self.left_rpm = int.from_bytes( ret[3:5], 'big', signed=True) * -0.1

        msg2 = [0x02, 0x03, 0x20, 0x2C, 0x00, 0x01, 0x4E, 0x30]
        self.ser.flushInput()
        self.send_message(msg2)
        self.ser.flushOutput()
        time.sleep(0.005)
        ret = self.read_message(7)
        time.sleep(0.005)
        self.right_rpm = int.from_bytes( ret[3:5], 'big', signed=True) * 0.1

        # if(self.right_rpm.hex()[3]==self.left_rpm.hex()[3]=='3'):
        #     print(self.right_rpm.hex())
        #     print(self.left_rpm.hex())
        #     print("------")

    
    def send_message(self,msg):
        self.ser.write(msg)

    def read_message(self,length):
        return self.ser.read(length)

    def tohex(self, val, nbits):
        return ((val + (1 << nbits)) % (1 << nbits)).to_bytes(2,'big')

if(__name__=="__main__"):
    
    ser = Serial_communicator()
    # a = [0x01, 0x06, 0x20, 0x3A, 0x00, 0x64, 0xA3, 0xEC]
    # print(type(a[0]))
    # ser.crc16(a)
    ser.send_rpm(1,100)
