#!/usr/bin/env python3

"""
Subscribe lidar LaserScan topic and filter out some value based on config
"""

import rospy
import rospkg
from rospy.exceptions import ROSException

from sensor_msgs.msg import LaserScan

import math
import numpy as np

import yaml

class laser_filter:
    def __init__(self, topic_in="scan", topic_out="scan_filtered", 
                config_path="{0}/config/lidar_angle.yaml".format(rospkg.RosPack().get_path('agv_control')) ) -> None:
        
        self.angles = []
        self.mode = 'rad'

        ### Lidar Params ###
        self.angle_min = 0
        self.angle_max = 2*math.pi
        self.angle_increment = 0
        
        self.time_increment = 0.0
        self.scan_time = 0.0
        self.range_min = 0.15
        self.range_max = 12.0
        
        self.params_set = False
        ### ### ###

        self.msg = LaserScan()

        
        ### get lidar topic and angle to remove ###
        with open(config_path, "r") as stream:
            try:
                config = yaml.safe_load(stream)
                if('mode' in config.keys()):
                    self.mode = config['mode']
                if('angles' in config.keys()):
                    self.angles = config['angles']
                if('topic_in' in config.keys()):
                    topic_in = config['topic_in']
                if('topic_out' in config.keys()):
                    topic_out = config['topic_out']
                
            except yaml.YAMLError as exc:
                print(exc)
        ### ### ### ###

        ### if angle mode is deg, change angle to rad
        if(self.mode=='deg'):
            for i, angles in enumerate(self.angles):
                for j, angle in enumerate(angles):
                    self.angles[i][j] = math.radians(self.angles[i][j])
            self.mode = 'rad'
        print(self.angles)
        ### ### ### ###


        self.sub = rospy.Subscriber(topic_in, LaserScan, self.laser_callback)
        self.pub = rospy.Publisher(topic_out, LaserScan, queue_size=10)

        pass

    def laser_callback(self,data):
        """ callback to lidar topic """
        if(not self.params_set):
            self.get_lidar_param(data)
        
        self.msg = data
        self.filter_data(data)

        self.pub.publish(self.msg)

    def get_lidar_param(self,data):
        self.angle_min = self.msg.angle_min = data.angle_min
        self.angle_max = self.msg.angle_max = data.angle_max
        self.angle_increment = self.msg.angle_increment = data.angle_increment
        self.time_increment = self.msg.time_increment = data.time_increment
        self.scan_time = self.msg.scan_time = data.scan_time
        self.range_min = self.msg.range_min = data.range_min
        self.range_max = self.msg.range_max = data.range_max
        self.params_set = True

    def filter_data(self,data):
        """ remove value from LaserScan ranges according to input angle """
        ranges = list(data.ranges)
        for angles in self.angles:
            ### get ranges array index to filter out
            start_index = int(angles[0]/self.angle_increment)
            stop_index = int(angles[1]/self.angle_increment)
            ### ### ###
            ranges[start_index:stop_index+1] = [math.inf] * (stop_index-start_index+1)

        self.msg.ranges = ranges
        pass



if(__name__=="__main__"):
    rospy.init_node("filter_lidar")
    laser_filter()    
    rospy.spin()
