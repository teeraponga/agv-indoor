#!/usr/bin/env python3
"""
publish recorded camera info of realsense d455 
"""

from sensor_msgs.msg import CameraInfo, Image
import rospy

msg = CameraInfo()

resolution = "848x480"
hz = 30
topic = "/camera/color/camera_info"

if(resolution=="424x240"):
    ################ FOR 424 x 240 resolution #####################
    msg.header.frame_id = "camera_color_optical_frame"
    msg.height = 240
    msg.width = 424
    msg.distortion_model = "plumb_bob"
    msg.D = [-0.05772596225142479, 0.07040407508611679, -0.00047655467642471194, -0.00027180498000234365, -0.022394174709916115]
    msg.K = [210.6272735595703, 0.0, 207.45362854003906, 0.0, 210.33328247070312, 119.59355926513672, 0.0, 0.0, 1.0]
    msg.R = [1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0]
    msg.P = [210.6272735595703, 0.0, 207.45362854003906, 0.0, 0.0, 210.33328247070312, 119.59355926513672, 0.0, 0.0, 0.0, 1.0, 0.0]
    ###############################################################

elif(resolution=="848x480"):
    ################ FOR 848 x 480 resolution #####################
    msg.header.frame_id = "camera_color_optical_frame"
    msg.height = 480
    msg.width = 848
    msg.distortion_model = "plumb_bob"
    msg.D = [-0.05772596225142479, 0.07040407508611679, -0.00047655467642471194, -0.00027180498000234365, -0.022394174709916115]
    msg.K = [423.43487548828125, 0.0, 414.9072570800781, 0.0, 422.8438720703125, 239.18711853027344, 0.0, 0.0, 1.0]
    msg.R = [1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0]
    msg.P = [423.43487548828125, 0.0, 414.9072570800781, 0.0, 0.0, 422.8438720703125, 239.18711853027344, 0.0, 0.0, 0.0, 1.0, 0.0]
    ###############################################################

def cb(data):
    msg.header.stamp = data.header.stamp

if(__name__=="__main__"):
    rospy.init_node("camera_info")
    rate = rospy.Rate(hz)
    pub = rospy.Publisher(topic, CameraInfo, queue_size=10)
    rospy.Subscriber("/camera/depth_registered/image_raw", Image, cb)
    print("started publishing...")
    while(not rospy.is_shutdown()):
        # msg.header.stamp = rospy.Time.now()
        pub.publish(msg)
        rate.sleep()
