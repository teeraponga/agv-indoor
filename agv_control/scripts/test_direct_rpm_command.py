#!/usr/bin/env python3


import rospy
import time
from geometry_msgs.msg import Twist, TwistStamped
        
if(__name__=="__main__"):
    rospy.init_node('test_direct_drive')
    
    rate = rospy.Rate(10)
    count = 1

    pub = rospy.Publisher("/cmd_vel", Twist, queue_size=10)
    msg = Twist()

    ### 1: rotate only ###
    msg.angular.z = 0.3
    for i in range(20):
        pub.publish(msg)
        rate.sleep()
    
    ### 2: forward ###
    msg.linear.x = 0.5
    for i in range(20):
        pub.publish(msg)
        rate.sleep()

    
    msg.linear.x = 0.0
    for i in range(20):
        pub.publish(msg)
        rate.sleep()

    msg.linear.x = -0.5
    for i in range(20):
        pub.publish(msg)
        rate.sleep()

    msg = Twist()
    pub.publish(msg)
    time.sleep(0.1)
    pub.publish(msg)
    pub.publish(msg)
    pub.publish(msg)
    pub.publish(msg)